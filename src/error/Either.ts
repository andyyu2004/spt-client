export interface Either<L, R> {
  /** If left, then no change; if right, then apply f to the contained value */
  map<T>(f: (x: R) => T): Either<L, T>;
  /** Similar to map, but applies on left instead of right */
  mapLeft<T>(f: (x: L) => T): Either<T, R>;
  bind<T>(f: (x: R) => Either<L, T>): Either<L, T>;
  match<T>(f: (l: L) => T, g: (r: R) => T): T;
  isLeft(): boolean;
  isRight(): boolean;
  /** Unwraps right */
  unwrap(): R;
  /** Unwraps the left */
  err(): L;
  unwrapOrElse(f: (l: L) => R): R;
}

export class Right<L, R> implements Either<L, R> {
  constructor(private rval: R) {}

  map<T>(f: (x: R) => T): Either<L, T> {
    return new Right<L, T>(f(this.rval));
  }

  mapLeft<T>(f: (x: L) => T): Either<T, R> {
    return new Right(this.rval);
  }

  bind<T>(f: (x: R) => Either<L, T>): Either<L, T> {
    return f(this.rval);
  }

  match<T>(f: (l: L) => T, g: (r: R) => T): T {
    return g(this.rval);
  }

  isLeft(): boolean {
    return false;
  }

  isRight(): boolean {
    return true;
  }

  unwrap(): R {
    return this.rval;
  }

  err(): never {
    throw new Error("Expected Left, found Right");
  }

  unwrapOrElse(f: (l: L) => R): R {
    return this.rval;
  }
}

export class Left<L, R> implements Either<L, R> {
  constructor(private lval: L) {}

  map<T>(f: (x: R) => T): Either<L, T> {
    return new Left(this.lval);
  }

  mapLeft<T>(f: (x: L) => T): Either<T, R> {
    return new Left(f(this.lval));
  }

  bind<T>(f: (x: R) => Either<L, T>): Either<L, T> {
    return new Left(this.lval);
  }

  match<T>(f: (l: L) => T, g: (r: R) => T): T {
    return f(this.lval);
  }

  isLeft(): boolean {
    return true;
  }

  isRight(): boolean {
    return false;
  }

  unwrap(): never {
    throw new Error("Unwrap of left either!");
  }

  err() {
    return this.lval;
  }

  unwrapOrElse(f: (l: L) => R): R {
    return f(this.lval);
  }
}
