import React, { useState } from "react";
import mstyles from "./toggle.module.scss";
import assert from "assert";
import { allUnique } from "../../util/array";

type Props = {
  options: string[];
  initialIndex: number;
} & React.ObjectHTMLAttributes<HTMLDivElement>;

const Toggle: React.FC<Props> = ({ initialIndex, options, ...props }) => {
  assert(allUnique(options));
  const [index, setIndex] = useState(initialIndex);

  return (
    <div {...props}>
      <div
        className={mstyles.button}
        onClick={() => setIndex(i => (i + 1) % options.length)}
      >
        {options.map((option, i) => {
          const styles: React.CSSProperties =
            i === index
              ? { backgroundColor: "#555", fontWeight: "bold" }
              : { backgroundColor: "#333" };

          return (
            <button className={mstyles.toggle} key={option} style={styles}>
              {option}
            </button>
          );
        })}
      </div>
    </div>
  );
};

export default Toggle;
