import React from "react";
import { Rule } from "../rules/types";
import { Draggable, Droppable, DragDropContext, DropResult } from "react-beautiful-dnd";
import styles from "./draggable-list.module.scss";

type Props = {
  droppableId: number;
  rules: Rule[];
  editRule: (rule: Rule) => void;
  update: (rule: Rule[]) => void;
  deleteRule: (id: number) => void;
};

const DraggableRulesList: React.FC<Props> = ({
  deleteRule,
  editRule,
  update,
  droppableId,
  rules,
}) => {
  const onDragEnd = (result: DropResult) => {
    const { source, destination } = result;
    if (destination?.index === undefined) return;

    /* case when destination.index < source.index
      0 a  drag 2 to 0 -> 0 c
      1 b              -> 1 a
      2 c              -> 2 b
      3 d              -> 3 d
      case when source.index < destination.index
      0 a  drag 0 to 2 -> 0 b
      1 b              -> 1 c
      2 c              -> 2 a
      3 d              -> 3 d */

    if (source.index > destination.index) {
      rules.slice(destination.index, source.index).forEach(rule => rule.rank++);
    } else if (source.index < destination.index) {
      rules.slice(1 + source.index, 1 + destination.index).forEach(rule => rule.rank--);
    }
    rules[source.index].rank = destination.index;
    update(rules);
  };

  return (
    <div className={styles.container}>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId={droppableId.toString()}>
          {(provided, _snapshot) => (
            <ol {...provided.droppableProps} ref={provided.innerRef}>
              {rules.map((rule, index) => (
                <Draggable key={rule.id} draggableId={rule.id.toString()} index={index}>
                  {(provided, _snapshot) => (
                    <li
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                    >
                      <button onClick={() => editRule(rule)}>{rule.name}</button>
                      <button className={"x-button"} onClick={() => deleteRule(rule.id)}>
                        X
                      </button>
                    </li>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </ol>
          )}
        </Droppable>
      </DragDropContext>
    </div>
  );
};

export default DraggableRulesList;
