import React from "react";
import styles from "./select-button.module.scss";

interface Props {
  activeStyle?: React.CSSProperties;
  style?: React.CSSProperties;
  active: boolean;
  onClick?: () => void;
}

/**
 * wrapper for button that takes additional prop 'active'
 * this can be used as a toggleable button
 */
const SelectButton = (props: React.PropsWithChildren<Props>) => {
  // must declare types in this way to work with default props :/
  const { children, active, activeStyle: propActiveStyle, style: propStyle, onClick } = props;
  const activeStyle: React.CSSProperties = {
    color: "#1CA6FC",
    ...propActiveStyle,
  };

  const style = {
    ...propStyle,
  };

  return (
    <button style={active ? activeStyle : style} className={styles.select_button} onClick={onClick}>
      {children}
    </button>
  );
};

SelectButton.defaultProps = { active: false };

export default SelectButton;
