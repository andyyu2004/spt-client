import { Time } from "../../util/time";
import { IdInterval } from "./forms/threshold";

export type RuleKind = TimeRule | ThresholdRule;

export type Minutes = number;

export class InitRuleInfo {
  constructor(
    public name: string,
    public mzone_id: number,
    public rank: number,
    public duration: Minutes,
    public withholding_time: Minutes,
    public kind: RuleKind
  ) {}
}

export class Rule {
  constructor(
    public id: number,
    public name: string,
    public mzone_id: number,
    public rank: number,
    public duration: Minutes,
    public withholding_time: Minutes,
    public last_executed: number,
    public kind: RuleKind
  ) {}

  public static fromJson(rule: Rule): Rule {
    const { id, name, mzone_id, duration, withholding_time, rank, kind, last_executed } = rule;
    console.log(kind);
    return new Rule(
      id,
      name,
      mzone_id,
      rank,
      duration,
      withholding_time,
      last_executed,
      Rule.kindFromJson(kind)
    );
  }

  /** reconstructs a rulekind from json object */
  public static kindFromJson(kind: RuleKind): RuleKind {
    switch (kind.type) {
      case "Time":
        return new TimeRule(Time.fromJson(kind.time));
      case "Threshold":
        const activeHours = kind.active_hours.map(Interval.fromJson);
        return new ThresholdRule(kind.threshold, activeHours);
    }
  }
}

// call the discriminating field "type" not "kind" for serde internally tagged deserializaton
export class TimeRule {
  public readonly type = "Time";
  constructor(public time: Time) {}
}

export class ThresholdRule {
  public readonly type = "Threshold";
  constructor(public threshold: number, public active_hours: Interval[]) {}
}

export enum Day {
  Mon = "Mon",
  Tue = "Tue",
  Wed = "Wed",
  Thu = "Thu",
  Fri = "Fri",
  Sat = "Sat",
  Sun = "Sun",
}

/** converts zero indexed number to day of the week */
export function dayToInt(day: Day): number {
  switch (day) {
    case Day.Mon:
      return 0;
    case Day.Tue:
      return 1;
    case Day.Wed:
      return 2;
    case Day.Thu:
      return 3;
    case Day.Fri:
      return 4;
    case Day.Sat:
      return 5;
    case Day.Sun:
      return 6;
    default:
      throw new Error("invalid integer representation of day");
  }
}

/** converts zero indexed number to day of the week */
export function intToDay(i: number): Day {
  switch (i) {
    case 0:
      return Day.Mon;
    case 1:
      return Day.Tue;
    case 2:
      return Day.Wed;
    case 3:
      return Day.Thu;
    case 4:
      return Day.Fri;
    case 5:
      return Day.Sat;
    case 6:
      return Day.Sun;
    default:
      throw new Error(`invalid integer representation of day ${i}`);
  }
}

export class Interval {
  constructor(public day: Day, public start: Time, public end: Time) {}

  public toUTC() {
    this.start.toUTC();
    this.end.toUTC();
  }

  /** this assumes that the json was in UTC and converts it back to local time */
  public static fromJson(interval: Interval) {
    const { day, start, end } = interval;
    return new Interval(day, Time.fromJson(start), Time.fromJson(end));
  }

  public static fromMomentInterval(interval: IdInterval): Interval {
    return new Interval(
      intToDay(interval.start.day()),
      Time.fromMoment(interval.start),
      Time.fromMoment(interval.end)
    );
  }
}
