import React, { useState } from "react";
import { Time } from "../../../util/time";
import { Grid } from "../../../views/layout/types";
import { TimeInput } from "../../inputs";
import { InitRuleInfo, TimeRule, Rule } from "../types";
import SharedForm from "./shared";
import assert from "assert";
import { RuleFormKind } from "..";

type Props = {
  grid: Grid;
  onUpdate?: (rule: Rule) => void;
  onSubmit?: (initInfo: InitRuleInfo) => void;
  initRule?: Rule;
};

/** either pass onSubmit or both onUpdate and initialRule
 * if both are passed, onSubmit takes precedence
 */
const TimeRuleForm: React.FC<Props> = ({ grid, onSubmit, initRule, onUpdate }) => {
  const { selectedMZone, rules } = grid;

  initRule && assert(initRule?.kind.type === RuleFormKind.Time);
  assert(onSubmit || (onUpdate && initRule));
  const rulekind = initRule?.kind as TimeRule;

  const [startTime, setStartTime] = useState(rulekind?.time.toString() ?? "11:30:00");
  const [duration, setDuration] = useState(initRule?.duration.toString() ?? "30");
  const [name, setName] = useState(initRule?.name ?? "timerule 1");
  const [withholdingTime, setWithholdingTime] = useState(
    initRule?.withholding_time.toString() ?? "0"
  );

  const handleSubmitForm = async (e: React.FormEvent) => {
    e.preventDefault();
    const d = parseInt(duration);
    const w = parseInt(withholdingTime);
    const time = Time.fromStr(startTime);
    time.toUTC();

    if (onSubmit) {
      const rule = new InitRuleInfo(
        name,
        selectedMZone!.id,
        rules.filter(r => r.mzone_id === selectedMZone?.id).length,
        d,
        w,
        new TimeRule(time)
      );
      onSubmit(rule);
    } else {
      const rule = new Rule(
        initRule!.id,
        name,
        selectedMZone!.id,
        initRule!.rank,
        d,
        w,
        initRule!.last_executed,
        new TimeRule(time)
      );
      onUpdate!(rule);
    }
  };

  return (
    <form onSubmit={handleSubmitForm}>
      <SharedForm
        name={name}
        duration={duration}
        withholdingTime={withholdingTime}
        setName={setName}
        setDuration={setDuration}
        setWithholdingTime={setWithholdingTime}
      />
      <label htmlFor="rule-form-start-time-input">Start Time (HH:MM:SS)</label>
      <TimeInput
        id="rule-form-start-time-input"
        value={startTime}
        onChange={e => setStartTime(e.target.value)}
      />
      <input type="submit" />
    </form>
  );
};

export default TimeRuleForm;
