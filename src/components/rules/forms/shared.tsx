import React from "react";
import { PosNumInput } from "../../inputs";

type Props = {
  duration: string;
  setDuration: React.Dispatch<React.SetStateAction<string>>;
  name: string;
  setName: React.Dispatch<React.SetStateAction<string>>;
  withholdingTime: string;
  setWithholdingTime: React.Dispatch<React.SetStateAction<string>>;
};

const SharedForm: React.FC<Props> = ({
  name,
  setName,
  duration,
  setDuration,
  withholdingTime,
  setWithholdingTime,
}) => (
  <>
    <label htmlFor="rule-form-name-input">Name</label>
    <input
      required
      id="rule-form-name-input"
      type="text"
      name="name"
      placeholder="name"
      value={name}
      onChange={(e) => setName(e.target.value)}
    />
    <label htmlFor="rule-form-duration-input">Duration</label>
    <PosNumInput
      id="rule-form-duration-input"
      placeholder="duration"
      value={duration}
      onChange={(e) => setDuration(e.target.value)}
    />
    <label htmlFor="rule-form-withholding-time-input">
      Withholding Duration
    </label>
    <PosNumInput
      id="rule-form-withholding-time-input"
      placeholder="withholding time"
      value={withholdingTime}
      onChange={(e) => setWithholdingTime(e.target.value)}
    />
  </>
);

export default SharedForm;
