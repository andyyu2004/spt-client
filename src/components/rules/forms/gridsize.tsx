import React, { useState } from "react";
import { Grid } from "../../../views/layout/types";
import { setGridDimensionAsync } from "../../../views/layout/actions";
import { DIM_STEP } from "../../../views/layout/constants";
import { GRAPH_SCALE } from "../../../views/layout/layout/constants";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";

const GridSizeForm: React.FC<{
  grid: Grid;
}> = ({ grid }) => {
  const dispatch = useDispatch();
  const { rect } = grid;

  // we use strings here so partially entered input that is not a valid number
  // (such as a minus sign) will still be enterable by the user
  const [minx, setminx] = useState((rect.minx / GRAPH_SCALE).toString());
  const [maxx, setmaxx] = useState((rect.maxx / GRAPH_SCALE).toString());
  const [miny, setminy] = useState((rect.miny / GRAPH_SCALE).toString());
  const [maxy, setmaxy] = useState((rect.maxy / GRAPH_SCALE).toString());

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    // multiply by graph scale because we want these numbers to be in terms of pixels, but when entered they are in terms of units
    const scaled = {
      minx: parseInt(minx) * GRAPH_SCALE,
      miny: parseInt(miny) * GRAPH_SCALE,
      maxx: parseInt(maxx) * GRAPH_SCALE,
      maxy: parseInt(maxy) * GRAPH_SCALE,
    };

    if (minx >= maxx || miny >= maxy)
      return toast.error("Minimum coordinates must be strictly less than the max");

    if (!grid.withinBound(scaled))
      return toast.error("Not all zones and sensors will be contained within the resized grid");

    setGridDimensionAsync(dispatch)(grid.id, scaled);
  };

  return (
    <div className="v-flex-container">
      <h3>Set Grid Size</h3>
      <form onSubmit={handleSubmit}>
        <input
          step={DIM_STEP}
          placeholder="minx"
          value={minx}
          type="number"
          onChange={e => setminx(e.target.value)}
        />
        <input
          step={DIM_STEP}
          placeholder="maxx"
          value={maxx}
          type="number"
          onChange={e => setmaxx(e.target.value)}
        />
        <input
          step={DIM_STEP}
          placeholder="miny"
          value={miny}
          type="number"
          onChange={e => setminy(e.target.value)}
        />
        <input
          step={DIM_STEP}
          placeholder="maxy"
          value={maxy}
          type="number"
          onChange={e => setmaxy(e.target.value)}
        />
        <input type="submit" />
      </form>
    </div>
  );
};

export default GridSizeForm;
