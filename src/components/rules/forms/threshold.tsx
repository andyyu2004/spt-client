import React, { useState } from "react";
import "react-week-calendar/dist/style.css";
import { v4 } from "uuid";
import { Grid } from "../../../views/layout/types";
import { ThresholdRule, InitRuleInfo, Rule, Interval } from "../types";
import { SharedRuleForm } from ".";
import { MZoneGraph } from "../../statistics";
import assert from "assert";
import moment, { Moment } from "moment";
import WeekCalendar from "react-week-calendar";

type Props = {
  onSubmit?: (initInfo: InitRuleInfo) => void;
  onUpdate?: (rule: Rule) => void;
  grid: Grid;
  initRule?: Rule;
};

// some arbitrary reference day as we only care about the weekday, hour, and minute
// we choose `referenceDay` below as our reference point.
// `referenceDay` is a Monday.
export const referenceDay = { y: 1, M: 0, D: 1, h: 0, m: 0, s: 0 };

// default intervals are the intervals that includes everything
// i.e. always active
const defaultIntervals = [0, 1, 2, 3, 4, 5, 6, 7].map(i => ({
  id: v4(),
  start: moment(referenceDay).add(i, "days"),
  end: moment(referenceDay).add(i, "days").add(23, "hours").add(59, "minutes"),
}));

/// interval with id
export type IdInterval = MomentInterval & {
  id: string;
};

export type MomentInterval = {
  start: Moment;
  end: Moment;
};

/** either pass onSubmit or both onUpdate and initialRule */
const ThresholdRuleForm: React.FC<Props> = ({ onSubmit, onUpdate, grid, initRule }) => {
  const { rules, selectedMZone } = grid;

  initRule && assert(initRule.kind.type === "Threshold");
  assert(onSubmit || (onUpdate && initRule));
  const rulekind = initRule?.kind as ThresholdRule;

  // the rest of the system has a slightly different representation of intervals
  // from the WeekCalendar and so we must convert to and fro
  const initIntervals = rulekind?.active_hours.map(x => ({
    id: v4(),
    start: x.start.toMoment(x.day),
    end: x.end.toMoment(x.day),
  }));

  const [threshold, setThreshold] = useState<string>(rulekind?.threshold.toString() ?? "");
  const [selectedIntervals, setSelectedIntervals] = useState<IdInterval[]>(
    initIntervals ?? defaultIntervals
  );
  const [showSchedule, setShowSchedule] = useState(false);
  const [showMoisture, setShowMoisture] = useState(false);

  const [name, setName] = useState<string>(initRule?.name ?? "");
  const [duration, setDuration] = useState<string>(initRule?.duration.toString() ?? "");
  const [withholdingTime, setWithholdingTime] = useState(
    initRule?.withholding_time.toString() ?? ""
  );

  const handleSubmitForm = async (e: React.FormEvent) => {
    e.preventDefault();
    const d = parseInt(duration);
    const w = parseInt(withholdingTime);
    const activeHours = selectedIntervals.map(i => Interval.fromMomentInterval(i));
    activeHours.forEach(i => i.toUTC());
    if (onSubmit) {
      const rule = new InitRuleInfo(
        name,
        selectedMZone!.id,
        // add the new rule as lowest priority
        rules.filter(r => r.mzone_id === selectedMZone?.id).length,
        d,
        w,
        new ThresholdRule(parseInt(threshold), activeHours)
      );
      onSubmit(rule);
    } else {
      const rule = new Rule(
        initRule!.id,
        name,
        selectedMZone!.id,
        initRule!.rank,
        d,
        w,
        initRule!.last_executed,
        new ThresholdRule(parseInt(threshold), activeHours)
      );
      onUpdate!(rule);
    }
  };

  const handleIntervalSelected = (newIntervals: MomentInterval[]) => {
    const intervals = newIntervals.map(interval => ({ id: v4(), ...interval }));
    setSelectedIntervals([...selectedIntervals, ...intervals]);
  };

  const handleIntervalRemoved = (interval: IdInterval) => {
    const index = selectedIntervals.findIndex(x => x.id === interval.id);
    selectedIntervals.splice(index, 1);
    setSelectedIntervals(selectedIntervals);
  };

  return (
    <div className="v-flex-container">
      <button
        onClick={() => {
          setShowSchedule(!showSchedule);
          setShowMoisture(false);
        }}
      >
        {showSchedule ? "Hide" : "Show"} Active Hours
      </button>
      {showSchedule && (
        <WeekCalendar
          firstDay={moment(referenceDay)}
          dayFormat="dddd"
          scaleUnit={60}
          selectedIntervals={selectedIntervals}
          cellHeight={20}
          useModal={false}
          onEventClick={handleIntervalRemoved}
          onIntervalSelect={handleIntervalSelected}
        />
      )}
      <button
        onClick={() => {
          setShowMoisture(!showMoisture);
          setShowSchedule(false);
        }}
      >
        {showMoisture ? "Hide" : "Show"} Moisture Graph
      </button>
      {showMoisture && <MZoneGraph mzoneId={selectedMZone!.id} />}
      <form className="v-flex-container" onSubmit={handleSubmitForm}>
        <SharedRuleForm
          name={name}
          duration={duration}
          withholdingTime={withholdingTime}
          setName={setName}
          setDuration={setDuration}
          setWithholdingTime={setWithholdingTime}
        />
        <label htmlFor="rule-form-threshold-limit">Threshold</label>
        <input
          id="rule-form-threshold-limit"
          type="number"
          placeholder="threshold"
          value={threshold}
          required
          onChange={e => setThreshold(e.target.value)}
        />
        <input type="submit" />
      </form>
    </div>
  );
};

export default ThresholdRuleForm;
