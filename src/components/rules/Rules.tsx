import React from "react";
import styles from "./rules.module.scss";
import { Grid } from "../../views/layout/types";
import { toast } from "react-toastify";
import { InitRuleInfo } from "./types";
import { addRuleAsync, GridAction } from "../../views/layout/actions";
import { setPopupContent, closePopup } from "../../redux/reducers/popup";
import { useDispatch } from "react-redux";
import { RuleForm } from ".";

