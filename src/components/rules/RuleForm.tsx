import React from "react";
import { Rule, InitRuleInfo } from "./types";
import { TimeRuleForm, ThresholdRuleForm } from "./forms";
import assert from "assert";
import { Grid } from "../../views/layout/types";

export enum RuleFormKind {
  Time = "Time",
  Threshold = "Threshold",
}

type Props = {
  grid: Grid;
  onSubmit?: (info: InitRuleInfo) => void;
  onUpdate?: (rule: Rule) => void;
  rulekind: RuleFormKind;
  initRule?: Rule;
};

const RuleForm: React.FC<Props> = ({ grid, rulekind, initRule, onUpdate, onSubmit }) => {
  // ensure a management zone is selected before rendering this
  assert(grid.selectedMZone !== undefined);
  switch (rulekind) {
    case RuleFormKind.Time:
      // don't use instanceof as when it is deserialized, kind is just a plain object not a class
      initRule && assert(initRule.kind.type === RuleFormKind.Time);
      return (
        <TimeRuleForm onSubmit={onSubmit} onUpdate={onUpdate} grid={grid} initRule={initRule} />
      );
    case RuleFormKind.Threshold:
      initRule && assert(initRule.kind.type === RuleFormKind.Threshold);
      return (
        <ThresholdRuleForm
          onSubmit={onSubmit}
          grid={grid}
          onUpdate={onUpdate}
          initRule={initRule}
        />
      );
    default:
      return <div>No Rule</div>;
  }
};

export default RuleForm;
