import React, { useState, useEffect } from "react";
import { apiFetchMZoneReadings } from "../../api";
import { toast } from "react-toastify";
import { Reading } from "../../views/layout/types";
import { WEEK_SECONDS, MoistureGraph } from ".";

type Props = {
  mzoneId: number;
  mintimestamp?: number;
};

export const MZoneGraph: React.FC<Props> = ({ mzoneId, mintimestamp }) => {
  const [readings, setReadings] = useState<Reading[]>([]);

  useEffect(() => {
    const getReadings = async () => {
      (await apiFetchMZoneReadings(mzoneId, mintimestamp ?? WEEK_SECONDS))
        .map(setReadings)
        .mapLeft(toast.error);
    };

    getReadings();
  }, [mzoneId, mintimestamp]);

  return <MoistureGraph readings={readings} />;
};

export default MZoneGraph;
