import React, { useEffect, useState } from "react";
import { apiFetchSensorReadings } from "../../api";
import { toast } from "react-toastify";
import { Reading } from "../../views/layout/types";
import { WEEK_SECONDS, MoistureGraph } from ".";

type Props = {
  sensorId: number;
  mintimestamp?: number;
};

const SensorGraph: React.FC<Props> = ({ sensorId, mintimestamp }) => {
  const [readings, setReadings] = useState<Reading[]>([]);

  useEffect(() => {
    const getReadings = async () => {
      const currentTime = Math.floor(new Date().getTime() / 1000);
      (await apiFetchSensorReadings(sensorId, mintimestamp ?? currentTime - WEEK_SECONDS))
        .map(setReadings)
        .mapLeft(toast.error);
    };
    getReadings();
  }, [sensorId, mintimestamp]);

  return <MoistureGraph title={`Sensor #${sensorId}`} readings={readings} />;
};

export default SensorGraph;
