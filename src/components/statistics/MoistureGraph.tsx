import React from "react";
import { Reading, Sensor } from "../../views/layout/types";
import {
  LineChart,
  XAxis,
  YAxis,
  Line,
  ResponsiveContainer,
  Tooltip,
  Legend,
  CartesianGrid,
} from "recharts";
import { useGrid } from "../../hooks";

type Props = {
  title?: string;
  readings: Reading[];
};

type MoistureData = { time: number; moisture?: number };

function calibrate(adc: number, low: number, hi: number): number {
  if (adc < low) adc = low;
  return (adc - low) / (hi - low);
}

/** renders a graph based on a list of readings */
const MoistureGraph: React.FC<Props> = ({ title, readings }) => {
  const mean = (xs: number[]) => xs.reduce((acc, x) => acc + x) / xs.length;
  const { sensors } = useGrid();

  const constructMap = (sensors: Sensor[]): Map<number, Sensor> => {
    const map = new Map();
    for (const sensor of sensors) map.set(sensor.id, sensor);
    return map;
  };

  const sensorMap = constructMap(sensors);

  const data = process(readings);

  /**
   * groups the readings by hour and takes the mean of all the readings
   * assume readings are chronologically ordered
   */
  function process(readings: Reading[]): MoistureData[] {
    if (readings.length === 0) return [];
    const INTERVAL = 3;
    // this assumes that the readings returned by the server is ordered from old readings to new
    const firstctime = readings[0].ctime;
    const latestctime = readings[readings.length - 1].ctime;

    const getHour = (ctime: number) => Math.floor((ctime - firstctime) / 3600);
    const maxHour = getHour(latestctime);

    const xs: number[][] = [];
    for (let i = 0; i <= Math.floor(maxHour / INTERVAL); i++) xs[i] = [];

    // fill in each index of the array with moisture readings
    readings.forEach(({ sensor_id, moisture, ctime }) => {
      const hour = Math.floor(getHour(ctime) / INTERVAL);
      const { low, high } = sensorMap.get(sensor_id)!;
      const calibrated = calibrate(moisture, low, high);
      const percentage = 100 * calibrated;
      xs[hour].push(percentage);
    });

    const means = xs.map(measurements => {
      if (measurements.length === 0) return undefined;
      return mean(measurements);
    });

    return means.map((moisture, i) => ({
      // shift the data so the latest time is at 0
      // all the division by INTERVAL stuff is so that the data is in 12 hour
      // intervals
      time: (i - Math.floor(maxHour / INTERVAL)) * INTERVAL,
      moisture,
    }));
  }

  return (
    <div style={{ height: "70vh", width: "70vw" }} className="v-flex-container">
      <h3>{title}</h3>
      <ResponsiveContainer height="90%" width="90%">
        <LineChart data={data}>
          <CartesianGrid stroke="#8882" strokeDasharray="5 5" />
          <Legend />
          <Tooltip />
          <XAxis
            dataKey="time"
            type="number"
            interval={5}
            tickCount={168}
            domain={[-168, 0]}
            allowDecimals
          />
          <YAxis />
          <Line type="monotone" dataKey="moisture" stroke="#8884d8" />
        </LineChart>
      </ResponsiveContainer>
    </div>
  );
};

export default MoistureGraph;
