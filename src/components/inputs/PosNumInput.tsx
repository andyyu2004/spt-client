import React from "react";

/** wrapper around input to only validate non-negative numbers; */
const NonNegNumInput: React.FC<React.InputHTMLAttributes<
  HTMLInputElement
>> = props => {
  const { step } = props;
  return <input {...props} type="number" min={0} required></input>;
};

export default NonNegNumInput;
