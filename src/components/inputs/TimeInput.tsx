import React from "react";

const TimeInput = (props: React.InputHTMLAttributes<HTMLInputElement>) => (
  <input
    required
    pattern="^([0-1][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$"
    placeholder="HH:MM:SS"
    {...props}
  />
);

export default TimeInput;
