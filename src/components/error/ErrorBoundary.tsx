import React from 'react'

type Props = {};
type State = {
    hadError: boolean;
};

export default class ErrorBoundary extends React.Component<Props, State> {
    constructor(props: any) {
	super(props);
	this.state = { hadError: false };
    }

    static getDerivedStateFromError(error: any) {
	return { hasError: true };
    }

    componentDidCatch(error: any, errorInfo: React.ErrorInfo) {
    }

    render() {
	if (this.state.hadError) {
	    return <h1>Something went wrong.</h1>;
	}

	return this.props.children;
    }
}
