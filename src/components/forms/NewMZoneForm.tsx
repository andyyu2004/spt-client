import React from "react";
import styles from "./forms.module.scss";
import { toast } from "react-toastify";
import { addMZoneAsync } from "../../views/layout/actions";
import { closePopup } from "../../redux/reducers/popup";
import { useDispatch } from "react-redux";
import { useGrid } from "../../hooks";

const NewMZoneForm: React.FC = () => {
  const grid = useGrid();
  const dispatch = useDispatch();
  const { mzones } = grid;
  const rdispatch = useDispatch();

  const handleSubmit = (e: any) => {
    e.preventDefault();
    const fd = new FormData(e.target);
    const mzoneName = fd.get("name") as string;
    if (mzones.map(m => m.name).includes(mzoneName))
      return toast.error(`Management zone with name \`${mzoneName}\` already exists`);
    addMZoneAsync(dispatch)(grid.id, mzoneName);
    rdispatch(closePopup());
  };

  return (
    <div className={styles.container}>
      <h4>New Management Zone</h4>
      <form onSubmit={handleSubmit}>
        <label htmlFor="new-mzone-name-input">Management Zone Name</label>
        <input type="text" id="new-mzone-name-input" name="name" required />
        <input type="submit" />
      </form>
    </div>
  );
};

export default NewMZoneForm;
