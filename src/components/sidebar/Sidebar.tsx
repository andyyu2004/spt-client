import React, { useState } from "react";
import assert from "assert";
import { allUnique, fst } from "../../util/array";
import styles from "./sidebar.module.scss";

type Props = {
  title?: string;
  subtitle?: string;
  subtitleAction?: () => void;
  items: [string, () => void][];
};

const Sidebar: React.FC<Props> = ({ title, items, subtitle, subtitleAction }) => {
  assert(allUnique(items.map(fst)));
  const [selected, setSelected] = useState<string>();

  return (
    <div className={styles.container}>
      <h3>{title}</h3>
      <button onClick={subtitleAction}>{subtitle}</button>
      <br />
      <br />
      {items.map(([name, f]) => (
        <button
          style={{ color: selected === name ? "lightgreen" : "white" }}
          className={styles.entry}
          key={name}
          onClick={() => {
            setSelected(name);
            f();
          }}
        >
          {name}
        </button>
      ))}
    </div>
  );
};

export default Sidebar;
