import React, { useState } from "react";
import styles from "./header.module.scss";
import assert from "assert";
import { allUnique } from "../../util/array";
import { navigate } from "@reach/router";

type Props = {
  title: string;
  items: [string, () => void][];
};

const Header: React.FC<Props> = ({ title, items }) => {
  assert(allUnique(items.map((x, _) => x)));
  const [selected, setSelected] = useState<string>();

  return (
    <header className={styles.header}>
      <span className={styles.title} onClick={() => navigate("/")}>
        {title}
      </span>
      {items.map(([name, f]) => (
        <button
          key={name}
          style={{ color: selected === name ? "lightgreen" : "white" }}
          className={styles.item}
          onClick={() => {
            setSelected(name);
            f();
          }}
        >
          {name}
        </button>
      ))}
    </header>
  );
};

export default Header;

