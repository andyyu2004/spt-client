import React, { useState } from "react";
import { RouteComponentProps } from "@reach/router";
import { useGrid } from "../../hooks";
import styles from "./calibration.module.scss";
import { Sensor } from "../layout/types";
import { updateSensor } from "../layout/actions";
import { useDispatch } from "react-redux";
import { apiUpdateSensor } from "../../api";
import { toast } from "react-toastify";

type Props = RouteComponentProps & { gridId: number };

const Calibration: React.FC<Props> = () => {
  const { sensors } = useGrid();
  const dispatch = useDispatch();
  /** map of sensor id to a bool representing if it requires an update to be sent to the server */
  const [hasChanged, setHasChanged] = useState<{ [key: number]: boolean }>({});

  const p = () => true;
  const filtered = sensors.filter(p);

  const handleLowChanged = (sensor: Sensor, low: number) => {
    if (sensor.low === low) return;
    sensor.low = low;
    setHasChanged({ [sensor.id]: true, ...hasChanged });
    dispatch(updateSensor(sensor));
  };

  const handleHighChanged = (sensor: Sensor, high: number) => {
    if (sensor.high === high) return;
    sensor.high = high;
    setHasChanged({ [sensor.id]: true, ...hasChanged });
    dispatch(updateSensor(sensor));
  };

  /** send the post request to the server */
  const putUpdateSensor = async (sensor: Sensor) => {
    if (!hasChanged[sensor.id]) return;
    if (isNaN(sensor.low)) return toast.error(`Invalid \`low\` entry for sensor #${sensor.id}`);
    if (isNaN(sensor.high)) return toast.error(`Invalid \`high\` entry for sensor #${sensor.id}`);
    if (sensor.low > sensor.high)
      return toast.error(`\`Low\` value must be less than \`High\` value`);

    setHasChanged({ [sensor.id]: false, ...hasChanged });

    (await apiUpdateSensor(sensor))
      .map(sensor => {
        // send the update to redux to ensure server and redux are synced
        updateSensor(sensor);
        console.log(sensor);
        return toast.success(`Updated sensor #${sensor.id}`);
      })
      .mapLeft(toast.error);
  };

  return (
    <div>
      <form>
        <table>
          <caption>
            <h3>Sensors</h3>
          </caption>
          <thead>
            <tr>
              <th>ID</th>
              <th>Type</th>
              <th>GridID</th>
              <th>SolenoidID</th>
              <th>Low</th>
              <th>High</th>
            </tr>
          </thead>
          <tbody>
            {filtered.map(s => (
              <tr key={s.id}>
                <td>{s.id}</td>
                <td>{s.kind}</td>
                <td>{s.grid_id}</td>
                <td>{s.szone_id ?? "N/A"}</td>
                <td>
                  <input
                    className={styles.input}
                    type="number"
                    step={1}
                    min={0}
                    max={isNaN(s.high) ? 0 : s.high}
                    value={s.low.toString()}
                    onBlur={() => putUpdateSensor(s)}
                    onChange={e => handleLowChanged(s, parseInt(e.target.value))}
                  />
                </td>
                <td>
                  <input
                    className={styles.input}
                    type="number"
                    min={isNaN(s.low) ? 0 : s.low}
                    max={65536}
                    step={1}
                    value={s.high.toString()}
                    onBlur={() => putUpdateSensor(s)}
                    onChange={e => handleHighChanged(s, parseInt(e.target.value))}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </form>
    </div>
  );
};

export default Calibration;
