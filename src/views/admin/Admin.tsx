import React from "react";
import { RouteComponentProps, navigate, Router } from "@reach/router";
import { GridPermissions, Invite } from "../layout/gridinfo";
import { Sidebar } from "../../components";
import { Calibration } from ".";
import { useFetchGrid, usePermissions } from "../../hooks";

// even if annotated as number it is still really a string
type Props = RouteComponentProps<{ gridIdStr: string }>;

const AdminHome: React.FC<RouteComponentProps> = () => {
  return <div>Admin</div>;
};

const Admin: React.FC<Props> = ({ gridIdStr }) => {
  const gridId = parseInt(gridIdStr!);
  useFetchGrid(gridId);
  const permissions = usePermissions(gridId);

  const sidebarItems: [string, () => void][] = [
    ["Invitations", () => navigate(`invitations`)],
    ["Calibration", () => navigate(`calibration`)],
  ];

  if (!(permissions & GridPermissions.Admin)) {
    navigate("/sites/");
    return null;
  }

  return (
    <div className="h-flex-container">
      <Sidebar
        title="Admin"
        subtitle="Return to site"
        subtitleAction={() => navigate("../")}
        items={sidebarItems}
      />
      <Router className="v-flex-container">
        <AdminHome path="/" />
        <Invite path="/invitations" gridId={gridId} />
        <Calibration path="/calibration" gridId={gridId} />
      </Router>
    </div>
  );
};

export default Admin;
