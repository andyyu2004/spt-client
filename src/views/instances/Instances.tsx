import React, { useEffect, useState } from "react";
import { RouteComponentProps, navigate, Router } from "@reach/router";
import { GridInfo } from "../../api/types";
import { fetchGridInfos, createNewGridInstance, deleteGrid } from "../../api";
import { toast } from "react-toastify";
import styles from "./instances.module.scss";
import { Designer } from "..";
import { useUserPermissions } from "../../hooks";
import { GridPermissions } from "../layout/gridinfo";

export const Instances: React.FC<RouteComponentProps> = () => {
  const [grids, setGrids] = useState<GridInfo[]>([]);

  const fetchGrids = async () => (await fetchGridInfos()).map(setGrids).mapLeft(toast.error);
  const userPermissions = useUserPermissions();

  useEffect(() => {
    fetchGrids();
  }, []);

  const handleCreateNewInstance = async () => {
    const name = prompt("Name of New Site: ");
    if (!name) return;
    (await createNewGridInstance(name))
      .map(grid => navigate(`/sites/${grid.id}`))
      .mapLeft(toast.error);
  };

  const handleDeleteInstance = async (grid_id: number, grid_name: string) => {
    const msg = `Are you sure you want to delete this site? This action is irreversible!`;
    if (!window.confirm(msg)) return;
    (await deleteGrid(grid_id))
      .map(() => toast.success(`Deleted Grid \`${grid_name}\``))
      .mapLeft(toast.error);
    fetchGrids();
  };

  const InstancesHome: React.FC<RouteComponentProps> = () => (
    <div className={styles["sidebar"]}>
      <h4>Sites</h4>
      <button onClick={handleCreateNewInstance}>Create New Site</button>
      <ul>
        {grids.map(({ grid_id, grid_name }) => {
          const hasAdminPermissions = (userPermissions[grid_id] & GridPermissions.Admin) !== 0;
          return (
            <li key={grid_id}>
              <button onClick={() => navigate(`${grid_id}`)}>{grid_name}</button>
              {hasAdminPermissions && (
                <button
                  className={"x-button"}
                  onClick={() => handleDeleteInstance(grid_id, grid_name)}
                >
                  X
                </button>
              )}
            </li>
          );
        })}
      </ul>
    </div>
  );

  return (
    <Router className="flex-container">
      <InstancesHome path="/" />
      <Designer path="/:gridIdStr/*" />
    </Router>
  );
};

export default Instances;
