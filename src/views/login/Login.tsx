import React, { useState } from "react";
import styles from "./login.module.scss";
import { RouteComponentProps, navigate } from "@reach/router";
import { apiLogin } from "../../api";
import { toast } from "react-toastify";
import { setCurrentUser } from "../../redux/reducers/user";
import { useDispatch } from "react-redux";

export const Login: React.FC<RouteComponentProps> = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();

  // on refresh, if redux still has the user information; send a request to ensure the session cookie is still present; if not log out
  // if redux has lost it, just force another login
  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const href = window.location.href;
    (await apiLogin(username, password))
      .map(user => {
        // set href to let the server deal with the routing
        // for example, this is important when someone copies in an invite link
        // they may need to login first, and then we want the server to handle the url not the
        // client which will just result in a 404
        window.location.href = href;
        return dispatch(setCurrentUser(user));
      })
      .mapLeft(toast.error);
  };

  return (
    <div className={styles.container}>
      <h3>Login</h3>
      <form onSubmit={handleSubmit} style={{ position: "relative" }}>
        <input
          className={styles["login-input"]}
          placeholder="Email/Username"
          value={username}
          onChange={e => setUsername(e.target.value)}
          required
          autoFocus
        />
        <input
          className={styles["login-input"]}
          placeholder="Password"
          type="password"
          value={password}
          onChange={e => setPassword(e.target.value)}
          required
        />
        <input type="submit" value="Login" />
      </form>
      <button onClick={() => navigate("/register")}>Register</button>
    </div>
  );
};

export default Login;
