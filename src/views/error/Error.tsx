import React from 'react'
import { RouteComponentProps } from '@reach/router';

type Props = RouteComponentProps & {
  code: number;
};

const Error: React.FC<Props> = ({ code }) => {
  return (
    <div>
      <h2>Error {code}</h2>
    </div>
  )
}

export default Error
