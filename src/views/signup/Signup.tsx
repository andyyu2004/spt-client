import React, { useState } from "react";
import styles from "./signup.module.scss";
import { RouteComponentProps, navigate } from "@reach/router";
import { register } from "../../api";
import { toast } from "react-toastify";
import { validateEmail } from "../../util/validation";

export const Signup: React.FC<RouteComponentProps> = () => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!validateEmail(email)) return toast.error("Please enter a valid email address");
    if (password !== password2) return toast.error("Password do not match");
    (await register(email, username, password))
      .map(user => {
        toast.success(`User \`${user.username}\` successfully created`);
        return navigate("/");
      })
      .mapLeft(toast.error);
  };

  return (
    <div className={styles.container}>
      <h3>Register</h3>
      <form onSubmit={handleSubmit}>
        <input
          className={styles["signup-input"]}
          placeholder="Email"
          value={email}
          onChange={e => setEmail(e.target.value)}
          required
        />
        <input
          className={styles["signup-input"]}
          placeholder="Username"
          value={username}
          onChange={e => setUsername(e.target.value)}
          required
        />
        <input
          className={styles["signup-input"]}
          placeholder="Password"
          type="password"
          value={password}
          onChange={e => setPassword(e.target.value)}
          required
        />
        <input
          className={styles["signup-input"]}
          placeholder="Confirm password"
          type="password"
          value={password2}
          onChange={e => setPassword2(e.target.value)}
          required
        />
        <input type="submit" />
      </form>
      <button onClick={() => navigate("/")}>Login</button>
    </div>
  );
};

export default Signup;
