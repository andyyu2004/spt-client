export { default as Designer } from "./layout/designer";
export { default as APITest } from "./settings";
export { default as Login } from "./login";
export { default as Signup } from "./signup";
export { default as Error } from "./error";
export { default as Home } from "./home";
export { default as Instances } from "./instances";
export { default as Admin } from "./admin";

