import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ReactSVGPanZoom, Tool, Value } from "react-svg-pan-zoom";
import { AutoSizer } from "react-virtualized";
import { AppState } from "../../../redux/reducers";
import { reduxSetValue } from "../../../redux/reducers/graphics";
import { compose } from "../../../util/functional";
import { toggleShapeSelection, updateSZoneAsync } from "../actions";
import { GRAPH_SCALE, DEFAULT_MZONE_NAME } from "../layout/constants";
import { Sensor, SZone } from "../types";
import styles from "./management.module.scss";
import { GridToolbar } from "..";
import { setPopupContent } from "../../../redux/reducers/popup";
import SensorGraph from "../../../components/statistics/sensor";
import { useGrid } from "../../../hooks";

const Management: React.FC = () => {
  const grid = useGrid();
  const { editMZoneId, selected, mzones, sensors, szones } = grid;

  const panzoomRef = useRef<any>();
  const rdispatch = useDispatch();
  const [tool, setTool] = useState<Tool>("pan");
  const translateX = (x: number): number => x - minx;
  const translateY = (y: number): number => y - miny;

  const defaultMZone = mzones.find(mzone => mzone.name === DEFAULT_MZONE_NAME)!;

  const dispatch = useDispatch();
  const value = useSelector<AppState, Value>(state => state.graphics.value);
  const { minx, miny, maxx, maxy } = grid.rect;

  const [gridWidth, gridHeight] = [maxx - minx, maxy - miny];

  const drawSZone = (szone: SZone) => {
    const { id, points } = szone;

    // render shape dependent on the mode (edit or normal)
    let stroke;
    if (editMZoneId) stroke = szone.mzone_id === editMZoneId ? "lightgreen" : "white";
    else stroke = szone === selected ? "orange" : "white";

    const handleEditModeSZoneDoubleClicked = () => {
      // if the szone already belongs to the `edit_mzone`, move it to `default mzone
      let newMZoneId;
      if (szone.mzone_id === editMZoneId) newMZoneId = defaultMZone.id;
      else newMZoneId = editMZoneId;
      updateSZoneAsync(dispatch)(new SZone(szone.id, newMZoneId!, szone.points));
    };

    let onDoubleClick;
    if (editMZoneId) onDoubleClick = handleEditModeSZoneDoubleClicked;
    else onDoubleClick = () => dispatch(toggleShapeSelection(szone));

    return (
      <polygon
        key={id}
        className={styles.szone}
        fill={mzones.find(m => m.id === szone.mzone_id)!.colour}
        stroke={stroke}
        strokeWidth={`${2 / value.a}px`}
        onDoubleClick={onDoubleClick}
        points={points.map(([x, y]) => [translateX(x), translateY(y)]).join(" ")}
      />
    );
  };

  const drawSensor = (sensor: Sensor) => {
    const { cx, cy, id } = sensor;
    const [x, y] = [translateX(cx), translateY(cy)];
    const handleDoubleClick = () => {
      rdispatch(setPopupContent(<SensorGraph sensorId={sensor.id} />));
    };

    return (
      <circle
        className={styles.sensor}
        cx={x}
        cy={y}
        key={id}
        r={GRAPH_SCALE / 3}
        fill="#f03005"
        onDoubleClick={handleDoubleClick}
      />
    );
  };

  return (
    <div className={styles.container}>
      {editMZoneId && <div className={styles.editbar}>Editing Management Zone #{editMZoneId}</div>}
      <AutoSizer>
        {({ width, height }) => (
          <ReactSVGPanZoom
            ref={panzoomRef}
            tool={tool}
            onChangeTool={setTool}
            value={value}
            scaleFactorMin={0.01}
            scaleFactorMax={3}
            onChangeValue={compose(dispatch, reduxSetValue)}
            SVGBackground="#333"
            background="#444"
            customToolbar={() => <GridToolbar panzoomRef={panzoomRef} />}
            width={width}
            height={height}
          >
            <svg width={gridWidth} height={gridHeight} stroke="red" strokeWidth="100">
              {szones.filter(szone => szone !== selected).map(drawSZone)}
              {selected && drawSZone(selected)}
              {sensors.filter(r => r.szone_id !== undefined).map(drawSensor)}
            </svg>
          </ReactSVGPanZoom>
        )}
      </AutoSizer>
    </div>
  );
};

export default Management;
