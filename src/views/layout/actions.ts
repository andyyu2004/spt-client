import { Rect, SZone, Sensor, Grid, MZone } from "./types";
import {
  apiDeleteSensor,
  apiDeleteSZone,
  apiInsertSZone,
  apiUpdateGridDimension,
  apiInsertSensor,
  apiInsertMZone,
  apiDeleteRule,
  apiAddRule,
  apiUpdateMZone,
  apiUpdateSZone,
  apiDeleteMZone,
  apiUpdateRule,
  apiUpdateSensor,
} from "../../api";
import { toast } from "react-toastify";
import { compose } from "../../util/functional";
import { Rule, InitRuleInfo } from "../../components/rules/types";

export type GridAction =
  | AddSZoneAction
  | ToggleSelectionAction
  | SetGridDimensionAction
  | DeleteSelectedAction
  | TransposeShapeAction
  | AddSensorAction
  | DragVertexAction
  | RemoveSensorAction
  | SetEditMZoneAction
  | ToggleSelectedSensorAction
  | SetGridAction
  | DragSensorAction
  | DeleteSensorAction
  | ReorderRulesAction
  | AddMZoneAction
  | ToggleSelectedMZoneAction
  | DeleteRuleAction
  | AddRuleAction
  | UpdateRuleAction
  | UpdateMZoneAction
  | UpdateSZoneAction
  | RecalculateSensorZones
  | UpdateSensorAction
  | UpdateGridAction;

export type UpdateRuleAction = {
  type: "UPDATE_RULE";
  rule: Rule;
};

export type UpdateSensorAction = {
  type: "UPDATE_SENSOR";
  sensor: Sensor;
};

export const updateSensor = (sensor: Sensor): UpdateSensorAction => ({
  type: "UPDATE_SENSOR",
  sensor,
});

export const updateRuleAsync = (dispatch: React.Dispatch<GridAction>) => async (rule: Rule) => {
  (await apiUpdateRule(rule)).map(compose(dispatch, updateRule)).mapLeft(toast.error);
};

export const updateRule = (rule: Rule): UpdateRuleAction => ({
  type: "UPDATE_RULE",
  rule,
});

export type RecalculateSensorZones = {
  type: "RECALCULATE_SENSOR_ZONES";
};

/** after resizing/moving or moving a sensor, recalculate which zone each sensor is in if any */
export const recalculateSensorZones = (): RecalculateSensorZones => ({
  type: "RECALCULATE_SENSOR_ZONES",
});

export type UpdateSZoneAction = {
  type: "UPDATE_SZONE";
  szone: SZone;
};

export const updateSZoneAsync = (dispatch: React.Dispatch<GridAction>) => async (szone: SZone) => {
  (await apiUpdateSZone(szone)).map(compose(dispatch, updateSZone)).mapLeft(toast.error);
};

const updateSZone = (szone: SZone): UpdateSZoneAction => ({
  type: "UPDATE_SZONE",
  szone,
});

export type UpdateMZoneAction = {
  type: "UPDATE_MZONE";
  mzone: MZone;
};

export const updateMZoneAsync = (dispatch: React.Dispatch<GridAction>) => async (mzone: MZone) => {
  (await apiUpdateMZone(mzone)).map(compose(dispatch, updateMZone)).mapLeft(toast.error);
};

const updateMZone = (mzone: MZone): UpdateMZoneAction => ({
  type: "UPDATE_MZONE",
  mzone,
});

export type AddRuleAction = {
  type: "ADD_RULE";
  rule: Rule;
};

export const addRuleAsync = (dispatch: React.Dispatch<GridAction>) => async (
  initInfo: InitRuleInfo
) => {
  (await apiAddRule(initInfo)).map(compose(dispatch, addRule)).mapLeft(toast.error);
};

const addRule = (rule: Rule): AddRuleAction => ({
  type: "ADD_RULE",
  rule,
});

export type DeleteRuleAction = {
  type: "DELETE_RULE";
  rule: Rule;
};

export const deleteRuleAsync = (dispatch: React.Dispatch<GridAction>) => async (ruleId: number) => {
  (await apiDeleteRule(ruleId)).map(compose(dispatch, deleteRule)).mapLeft(toast.error);
};

export const deleteRule = (rule: Rule): DeleteRuleAction => ({
  type: "DELETE_RULE",
  rule,
});

export type ToggleSelectedMZoneAction = {
  type: "TOGGLE_SELECTED_MZONE";
  mzone: MZone;
};

/** set which management zone we are editing */
export type SetEditMZoneAction = {
  type: "SET_EDIT_MZONE";
  editMZoneId?: number;
};

export const setEditMZone = (editMZoneId?: number): SetEditMZoneAction => ({
  type: "SET_EDIT_MZONE",
  editMZoneId,
});

export const toggleSelectedMZone = (mzone: MZone): ToggleSelectedMZoneAction => ({
  type: "TOGGLE_SELECTED_MZONE",
  mzone,
});

export type AddMZoneAction = {
  type: "ADD_MZONE";
  mzone: MZone;
};

export const addMZoneAsync = (dispatch: React.Dispatch<GridAction>) => async (
  gridId: number,
  name: string
) => {
  (await apiInsertMZone(gridId, name)).map(compose(dispatch, addMZone)).mapLeft(toast.error);
};

const addMZone = (mzone: MZone): AddMZoneAction => ({
  type: "ADD_MZONE",
  mzone,
});

export type ReorderRulesAction = {
  type: "REORDER_RULES";
  rules: Rule[];
};

// move rule at index init to index final
export const reorderRules = (rules: Rule[]): ReorderRulesAction => ({
  type: "REORDER_RULES",
  rules,
});

export type DeleteSelectedAction = { type: "DELETE_SELECTED" };
export const deleteSelected = (): DeleteSelectedAction => ({
  type: "DELETE_SELECTED",
});

export type DeleteMZoneAction = {
  type: "DELETE_MZONE";
  mzone: MZone;
};

export const deleteMZoneAsync = (dispatch: React.Dispatch<GridAction>) => async (mzone: MZone) => {
  (await apiDeleteMZone(mzone.id)).map(compose(dispatch, setGrid)).mapLeft(toast.error);
};

export type DeleteSensorAction = {
  type: "DELETE_SENSOR";
  sensor: Sensor;
};

export const deleteSensorAsync = (dispatch: React.Dispatch<GridAction>) => async (sensor: Sensor) =>
  (await apiDeleteSensor(sensor.id)).map(() => dispatch(deleteSensor(sensor))).mapLeft(toast.error);

const deleteSensor = (sensor: Sensor): DeleteSensorAction => ({
  type: "DELETE_SENSOR",
  sensor,
});

export type DeleteSZoneAction = {
  type: "DELETE_SZONE";
  szone: SZone;
};

export const deleteSZoneAsync = (dispatch: React.Dispatch<GridAction>) => async (szone: SZone) => {
  (await apiDeleteSZone(szone.id)).map(compose(dispatch, setGrid)).mapLeft(toast.error);
};

export type DragSensorAction = {
  type: "DRAG_SENSOR";
  id: number;
  cx: number;
  cy: number;
};

export const dragSensor = (id: number, cx: number, cy: number): DragSensorAction => ({
  type: "DRAG_SENSOR",
  id,
  cx,
  cy,
});

export type ToggleSelectedSensorAction = {
  type: "TOGGLE_SELECTED_SENSOR";
  sensor: Sensor;
};

export const toggleSelectedSensor = (sensor: Sensor): ToggleSelectedSensorAction => ({
  type: "TOGGLE_SELECTED_SENSOR",
  sensor,
});

export type AddSZoneAction = {
  type: "ADD_SZONE";
  szone: SZone;
};

export const addSZoneAsync = (dispatch: React.Dispatch<GridAction>) => async (
  gridId: number,
  szone: SZone
) => {
  (await apiInsertSZone(gridId, szone))
    .map(() => {
      dispatch(addSZone(szone));
      // need to recalculate as the szone may have been drawn on top of existing sensors
      return dispatch(recalculateSensorZones());
    })
    .mapLeft(toast.error);
};

export const addSZone = (szone: SZone): AddSZoneAction => ({
  type: "ADD_SZONE",
  szone,
});

export type AddSensorAction = {
  type: "ADD_SENSOR";
  sensor: Sensor;
};

export const addSensorAsync = (dispatch: React.Dispatch<GridAction>) => async (sensor: Sensor) => {
  (await apiInsertSensor(sensor)).map(compose(dispatch, addSensor)).mapLeft(toast.error);
};

export const addSensor = (sensor: Sensor): AddSensorAction => ({
  type: "ADD_SENSOR",
  sensor,
});

/** remove a sensor by its position */
export type RemoveSensorAction = {
  type: "REMOVE_SENSOR";
  position: number; // as a paired coordinate
};

export const removeSensor = (position: number): RemoveSensorAction => ({
  type: "REMOVE_SENSOR",
  position,
});

export type SetGridDimensionAction = {
  type: "SET_GRID_DIMENSION";
  rect: Rect;
};

export const setGridDimensionAsync = (dispatch: React.Dispatch<GridAction>) => async (
  gridId: number,
  rect: Rect
) => {
  (await apiUpdateGridDimension(gridId, rect))
    .map(() => dispatch(setGridDimension(rect)))
    .mapLeft(toast.error);
};

export const setGridDimension = (rect: Rect): SetGridDimensionAction => ({
  type: "SET_GRID_DIMENSION",
  rect,
});

export type UpdateGridAction = { type: "UPDATE_GRID" };

export const updateGrid = (): UpdateGridAction => ({ type: "UPDATE_GRID" });

export type ToggleSelectionAction = {
  type: "TOGGLE_SELECTION";
  szone: SZone;
};

export const toggleShapeSelection = (szone: SZone): ToggleSelectionAction => ({
  type: "TOGGLE_SELECTION",
  szone,
});

export type TransposeShapeAction = {
  type: "TRANSPOSE_SHAPE";
  id: number;
  dx: number;
  dy: number;
};

export const transposeShape = (id: number, dx: number, dy: number): TransposeShapeAction => ({
  type: "TRANSPOSE_SHAPE",
  id,
  dx,
  dy,
});

export type DragVertexAction = {
  type: "DRAG_VERTEX";
  id: number /* id of the szone */;
  idx: number;
  x: number;
  y: number;
};

export const dragVertex = (id: number, idx: number, x: number, y: number): DragVertexAction => ({
  type: "DRAG_VERTEX",
  id,
  idx,
  x,
  y,
});

export type SetGridAction = {
  type: "SET_GRID";
  grid: Grid;
};

export const setGrid = (grid: Grid): SetGridAction => ({
  type: "SET_GRID",
  grid,
});
