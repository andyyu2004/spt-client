import { RouteComponentProps, Router } from "@reach/router";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { compose } from "redux";
import { Admin } from "../..";
import { fetchUserGridPermissions } from "../../../api/";
import { useFetchGrid, usePermissions } from "../../../hooks";
import { setUserPermissions } from "../../../redux/reducers/user";
import { GridInfo, GridPermissions } from "../gridinfo";
import { Layout } from "../layout";
import Management from "../management";
import styles from "./designer.module.scss";

// even if annotated as number it is still actually a string
type Props = RouteComponentProps<{ gridIdStr: string }>;

export type DesignMode = "layout" | "manage";

/** top-level component that controls layout and holds the top-level layout state */
const Designer: React.FC<Props> = ({ gridIdStr }) => {
  const gridId = parseInt(gridIdStr!);
  useFetchGrid(gridId);

  const [mode, setMode] = useState<DesignMode>("manage");
  const dispatch = useDispatch();

  const permissions = usePermissions(gridId);

  const Content: React.FC<RouteComponentProps> = () => {
    return (
      <div className={styles.container}>
        {permissions & GridPermissions.Write ? (
          <GridInfo designMode={mode} setDesignMode={setMode} />
        ) : null}
        {permissions & GridPermissions.Read ? (
          <main className={styles.main}>{mode === "layout" ? <Layout /> : <Management />}</main>
        ) : null}
      </div>
    );
  };

  const setUserGridPermissions = useCallback(async () => {
    (await fetchUserGridPermissions()).map(compose(dispatch, setUserPermissions));
  }, [dispatch]);

  useEffect(() => {
    setUserGridPermissions();
  }, [setUserGridPermissions]);

  return (
    // note that a ternary operator is required (instead of just &&)
    // otherwise it renders `0` instead of nothing
    <Router className="flex-container">
      <Content path="/" />
      <Admin path="/admin/*" />
    </Router>
  );
};

export default Designer;
