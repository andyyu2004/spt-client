/** Some of these can probably be saved as options */

/** dimension of each grid (in metres) */
export const GRID_UNIT = 1;

export const ZOOM_SPEED = 0.0001;

/** granularity of dimensions */
export const DIM_STEP = 1;

export const POLYGON_REGEX = "(-?\\d+,-?\\d+\\s?){3,}";

/** Minimap selectors */
export const MINIMAP_PRIMARY_SELECTOR = "MINIMAP_PRIMARY_SELECTOR";
export const MINIMAP_SECONDARY_SELECTOR = "MINIMAP_SECONDARY_SELECTOR";