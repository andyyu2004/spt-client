import React from "react";
import { toast } from "react-toastify";
import { parsePoints } from "../../../util/parse";
import { GridAction, addSZoneAsync, setGridDimensionAsync } from "../actions";
import { DIM_STEP, POLYGON_REGEX } from "../constants";
import { Grid, SZone } from "../types";
import styles from "./grid-builder.module.scss";
import { GRAPH_SCALE } from "../layout/constants";
import { mzoneIdFromName } from "../../../util/validation";

type Props = {
  grid: Grid;
  dispatch: React.Dispatch<GridAction>;
};

const GridBuilder: React.FC<Props> = ({ grid, dispatch }) => {
  const { mzones } = grid;
  const { minx, miny, maxx, maxy } = grid.rect;

  const handleSetGridDimensions = (e: any) => {
    e.preventDefault();
    const fd = new FormData(e.target);
    // Can safely cast these due to the validation performed
    const [minx, miny, maxx, maxy] = [
      "grid-minx",
      "grid-miny",
      "grid-maxx",
      "grid-maxy",
    ].map(f => parseFloat(fd.get(f) as string) * GRAPH_SCALE);

    if (minx >= maxx || miny >= maxy)
      return toast.error(
        "Minimum coordinates must be strictly less than the max"
      );

    if (!grid.withinBound({ minx, maxx, miny, maxy }))
      return toast.error(
        "Not all zones and sensors will be contained within the resized grid"
      );
    // multiply by graph scale because we want these numbers to be in terms of pixels, but when entered they are in terms of units
    setGridDimensionAsync(dispatch)(grid.id, { minx, miny, maxx, maxy });
  };

  const handleAddPolygon = (e: any) => {
    e.preventDefault();
    const fd = new FormData(e.target);
    const polygonString = fd.get("polygon") as string;
    const id = parseInt(fd.get("id") as string);
    const mzoneName = fd.get("mzone") as string;
    const mzoneId = mzoneIdFromName(mzones, mzoneName);
    if (!mzoneId || typeof mzoneId === "string")
      return toast.error(`Cannot find mzone with name \`${mzoneName}\``);
    const points = parsePoints(polygonString);
    const szone = new SZone(id, mzoneId, points);
    if (!szone.withinBound(grid.rect))
      return toast.error("Polygon will not be within bounds");
    addSZoneAsync(dispatch)(grid.id, szone);
    e.target.reset();
  };

  return (
    <div className={styles.container}>
      <span>
        <b>Set Grid Dimensions</b>
      </span>
      <hr />
      <form onSubmit={handleSetGridDimensions}>
        <label htmlFor="grid-width-input">Grid Width: </label>
        <label htmlFor="grid-minx-input">Grid Min X: </label>
        <input
          id="grid-minx-input"
          name="grid-minx"
          step={DIM_STEP}
          defaultValue={minx / GRAPH_SCALE}
        />
        <label htmlFor="grid-miny-input">Grid Min Y: </label>
        <input
          id="grid-miny-input"
          name="grid-miny"
          step={DIM_STEP}
          defaultValue={miny / GRAPH_SCALE}
        />
        <label htmlFor="grid-maxx-input">Grid Max X: </label>
        <input
          id="grid-maxx-input"
          name="grid-maxx"
          step={DIM_STEP}
          defaultValue={maxx / GRAPH_SCALE}
        />
        <label htmlFor="grid-maxy-input">Grid Max Y: </label>
        <input
          id="grid-maxy-input"
          name="grid-maxy"
          step={DIM_STEP}
          defaultValue={maxy / GRAPH_SCALE}
        />
        <input type="submit" value="Set Dimension" />
      </form>
      <br />

      <span>
        <b>Insert New Solenoid Zone</b>
      </span>
      <hr />
      <form onSubmit={handleAddPolygon}>
        <label htmlFor="poly-id-input">ID: </label>
        <input
          id="poly-id-input"
          name="id"
          type="number"
          placeholder="ID"
          step={1}
          required
        />
        <input
          placeholder="Management Zone Name"
          id="mzone-input"
          name="mzone"
          required
        />
        <label className="tooltip" htmlFor="polygon-input">
          <span>Polygon: </span>
          <span className="tooltip-text">
            enter a space-separated list of 3-or-more comma-seperated pairs.
            e.g. 0,0 1,1 2,3
          </span>
        </label>
        <input
          id="polygon-input"
          name="polygon"
          placeholder="hover above for help"
          pattern={POLYGON_REGEX}
          required
        />
        <input type="submit" value="Add Polygon" />
      </form>
    </div>
  );
};

export default GridBuilder;
