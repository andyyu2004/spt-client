import { GridAction } from "./actions";
import { Grid } from "./types";
import { putUpdateGrid as putUpdatedGrid } from "../../api";
import { pointInPolygon } from "../../util/geometry";
import { toast } from "react-toastify";
import { DEFAULT_MZONE_NAME } from "./layout/constants";

type State = Grid;

const gridReducerWrapper = (state: State, action: GridAction): State => {
  console.log("action:", action);
  const newState = gridReducer(state, action);
  console.log("new state:", newState);
  return newState;
};

const gridReducer: (state: State, action: GridAction) => State = (state, action) => {
  const { mzones, selected, selectedMZone, rules, szones, sensors, selectedSensor } = state;
  switch (action.type) {
    case "UPDATE_GRID":
      // making rare (reluctant) choice to have an asynchronous request in a reducer
      // we cannot put `grid` as a dependency in useEffect as it will be called way too often
      // and will drown the server in PUT requests
      // Consequently, we cannot make the request from the useEffect hook as the grid will be out of date.
      // However, if it dispatches an action to the reducer the state will always be up to date
      putUpdatedGrid(state).then(either => either.mapLeft(console.log));
      return state;
    case "SET_GRID":
      return { ...state, ...action.grid };
    case "SET_GRID_DIMENSION": {
      const { rect } = action;
      return { ...state, rect };
    }
    case "SET_EDIT_MZONE": {
      const { editMZoneId } = action;
      return { ...state, editMZoneId: state.editMZoneId ? undefined : editMZoneId };
    }
    case "REORDER_RULES": {
      const { rules: updatedRules } = action;
      updatedRules.forEach(rule => {
        const i = rules.findIndex(r => r.id === rule.id)!;
        rules[i] = rule;
      });
      return { ...state, rules: rules.sort((x, y) => x.rank - y.rank) };
    }
    case "UPDATE_MZONE": {
      const { mzone: updatedMZone } = action;
      const mzone = mzones.find(m => m.id === updatedMZone.id)!;
      mzone.colour = updatedMZone.colour;
      mzone.name = updatedMZone.name;
      return { ...state, mzones };
    }
    case "UPDATE_SENSOR": {
      const { sensor: updatedSensor } = action;
      const i = rules.findIndex(r => r.id === updatedSensor.id)!;
      sensors[i] = updatedSensor;
      return {
        ...state,
        sensors,
      };
    }
    case "UPDATE_RULE": {
      const { rule: updatedRule } = action;
      const i = rules.findIndex(r => r.id === updatedRule.id)!;
      rules[i] = updatedRule;
      return {
        ...state,
        rules,
      };
    }
    case "UPDATE_SZONE": {
      const { szone: updatedSZone } = action;
      const szone = szones.find(s => s.id === updatedSZone.id)!;
      szone.points = updatedSZone.points;
      szone.mzone_id = updatedSZone.mzone_id;
      return { ...state, mzones };
    }
    case "ADD_RULE": {
      const { rule } = action;
      return {
        ...state,
        rules: [...rules, rule].sort((x, y) => x.rank - y.rank),
      };
    }
    case "DELETE_RULE": {
      const { rule } = action;
      rules.splice(
        rules.findIndex(r => r.id === rule.id),
        1
      );
      return { ...state, rules };
    }
    case "ADD_SZONE": {
      const { szone } = action;
      return { ...state, szones: [...szones, szone] };
    }
    case "TOGGLE_SELECTION": {
      const { szone } = action;
      const selected = state.selected === szone ? undefined : szone;
      // also select the management zone of the selected solenoid zone
      const selectedMZone = selected
        ? mzones.find(mzone => mzone.id === selected.mzone_id)
        : undefined;

      return {
        ...state,
        selected,
        // don't select the default mzone
        selectedMZone: selectedMZone?.name !== DEFAULT_MZONE_NAME ? selectedMZone : undefined,
        // deselect sensor
        selectedSensor: undefined,
      };
    }
    case "TOGGLE_SELECTED_SENSOR": {
      const { sensor } = action;
      return {
        ...state,
        selectedSensor: selectedSensor === sensor ? undefined : sensor,
        selected: undefined,
      };
    }
    case "RECALCULATE_SENSOR_ZONES": {
      sensors.forEach(sensor => {
        const szoneId = szones.find(s => pointInPolygon([sensor.cx, sensor.cy], s.points))?.id;
        sensor.szone_id = szoneId;
      });
      putUpdatedGrid(state).then(either => either.mapLeft(toast.error));
      return { ...state, sensors };
    }
    case "ADD_MZONE": {
      const { mzone } = action;
      return { ...state, mzones: [...mzones, mzone] };
    }
    case "TOGGLE_SELECTED_MZONE": {
      const { mzone } = action;
      return {
        ...state,
        editMZoneId: undefined,
        selectedMZone: selectedMZone === mzone ? undefined : mzone,
      };
    }
    case "DELETE_SENSOR": {
      const { sensor } = action;
      sensors.splice(sensors.indexOf(sensor), 1);
      return { ...state, sensors, selectedSensor: undefined };
    }
    case "DELETE_SELECTED": {
      selected && szones.splice(szones.indexOf(selected), 1);
      selectedSensor && sensors.splice(sensors.indexOf(selectedSensor), 1);
      return {
        ...state,
        sensors,
        szones,
        selected: undefined,
        selectedSensor: undefined,
      };
    }
    case "TRANSPOSE_SHAPE": {
      const { id, dx, dy } = action;
      const szone = szones.find(s => s.id === id)!;
      const transposed = szone.transpose(dx, dy);
      if (!transposed.withinBound(state.rect)) return state;
      szone.transposeInPlace(dx, dy);
      return { ...state, szones };
    }
    case "DRAG_VERTEX": {
      const { id, idx, x, y } = action;
      const szone = szones.find(s => s.id === id)!;
      if (szone.kind === "polygon") {
        szone.points[idx] = [x, y];
      }
      return { ...state, szones };
    }
    case "ADD_SENSOR": {
      const { sensor } = action;
      sensors.push(sensor);
      return { ...state, sensors };
    }
    case "REMOVE_SENSOR": {
      // const { position } = action;
      // sensors.delete(position);
      return { ...state, sensors };
    }
    case "DRAG_SENSOR": {
      const { id, cx, cy } = action;
      const sensor = sensors.find(s => s.id === id)!;
      sensor.cx = cx;
      sensor.cy = cy;
      return { ...state, sensors };
    }
    default:
      return state;
  }
};

export default gridReducerWrapper;
