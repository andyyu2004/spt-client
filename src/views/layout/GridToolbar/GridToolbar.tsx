import React from "react";
import { ReactSVGPanZoom } from "react-svg-pan-zoom";
import reset_icon from "../../../assets/reset_icon.png";
import zoom_in_icon from "../../../assets/zoom_in_icon.svg";
import zoom_out_icon from "../../../assets/zoom_out_icon.png";
import zoom_to_fit_icon from "../../../assets/zoom_to_fit_icon.png";
import { ZOOM_FACTOR } from "../layout/constants";
import styles from "./gridtoolbar.module.scss";

type Props = {
  panzoomRef: React.RefObject<ReactSVGPanZoom>;
};

const GridToolbar: React.FC<Props> = ({ panzoomRef }) => (
  <div className={styles["grid-toolbar"]}>
    <img
      src={zoom_in_icon}
      alt="zoom in"
      className={styles.toolbaricon}
      onClick={() => panzoomRef.current?.zoomOnViewerCenter(ZOOM_FACTOR)}
    />
    <img
      src={zoom_out_icon}
      alt="zoom out"
      className={styles.toolbaricon}
      onClick={() => panzoomRef.current?.zoomOnViewerCenter(1 / ZOOM_FACTOR)}
    />
    <img
      src={reset_icon}
      alt="reset"
      className={styles.toolbaricon}
      onClick={() => panzoomRef.current?.reset()}
    />
    <img
      src={zoom_to_fit_icon}
      alt="zoom to fit"
      className={styles.toolbaricon}
      onClick={() => panzoomRef.current?.fitToViewer()}
    />
  </div>
);

export default GridToolbar;
