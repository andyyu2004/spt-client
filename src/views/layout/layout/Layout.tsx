import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ReactSVGPanZoom, Tool as NavTool, Value } from "react-svg-pan-zoom";
import { AutoSizer } from "react-virtualized";
import { DrawingPad } from ".";
import { SelectButton } from "../../../components";
import { AppState } from "../../../redux/reducers";
import { reduxSetValue } from "../../../redux/reducers/graphics";
import { compose } from "../../../util/functional";
import { toggleShapeSelection } from "../actions";
import GridToolbar from "../GridToolbar/GridToolbar";
import styles from "./layout.module.scss";
import { useGrid } from "../../../hooks";

type Props = {
  style?: React.CSSProperties;
};

type SvgState = {
  tool: NavTool;
  value: Value;
};

export type Mode = "nav" | "draw";
export type DrawTool = "poly" | "sensor" | "none";

const Layout: React.FC<Props> = () => {
  const [svgState, setSvgState] = useState<SvgState>({
    value: {} as Value,
    tool: "pan",
  });

  const grid = useGrid();
  const dispatch = useDispatch();
  const value = useSelector<AppState, Value>(state => state.graphics.value);

  const { minx, miny, maxx, maxy } = grid.rect;
  const [gridWidth, gridHeight] = [maxx - minx, maxy - miny];

  const [mode, setMode] = useState<Mode>("nav");
  const setNavTool = (tool: NavTool) => setSvgState({ ...svgState, tool });
  const [drawTool, setDrawTool] = useState<DrawTool>("none");

  const containerRef = useRef<HTMLDivElement>(null);
  const panzoomRef = useRef<ReactSVGPanZoom>(null);
  const toolbarRef = useRef<any>(null);
  const drawingPadRef = useRef<any>();

  useEffect(() => {
    // when this effect is placed in drawing pad it is the handlers are always called twice for some reason
    // something to do with being placed inside the panzoom component
    const handleKeyDown = (e: KeyboardEvent) => {
      switch (e.key) {
        case "Delete":
          return drawingPadRef.current?.deleteSelected();
        case "Escape":
          return drawingPadRef.current?.cancelDrawing();
      }
    };

    document.addEventListener("keydown", handleKeyDown);
    return () => document.removeEventListener("keydown", handleKeyDown);
  });

  function toNavMode(tool: NavTool = "pan") {
    setMode("nav");
    setDrawTool("none");
    setNavTool(tool);
    // drawingPadRef.current?.cancelDrawing();
  }

  function toDrawMode(tool: DrawTool = "poly") {
    setMode("draw");
    setNavTool("none");
    // deselect before drawing to avoid handles getting in the way
    if (grid.selected) dispatch(toggleShapeSelection(grid.selected));
    // drawingPadRef.current?.cancelDrawing();
    setDrawTool(tool);
  }

  const Toolbar: React.FC = () => (
    <div className={styles.toolbar} ref={toolbarRef}>
      <h4 style={{ color: mode === "nav" ? "lightgreen" : "red" }}>Nav: </h4>
      <SelectButton active={svgState.tool === "pan"} onClick={() => toNavMode("pan")}>
        Pan
      </SelectButton>
      <SelectButton active={svgState.tool === "zoom-in"} onClick={() => toNavMode("zoom-in")}>
        Zoom In
      </SelectButton>
      <hr className="v-sep" />

      <h4 style={{ color: mode === "draw" ? "lightgreen" : "red" }}>Draw: </h4>
      <SelectButton active={drawTool === "poly"} onClick={() => toDrawMode("poly")}>
        Irrigation Zone
      </SelectButton>
      <SelectButton active={drawTool === "sensor"} onClick={() => toDrawMode("sensor")}>
        Sensor
      </SelectButton>
      <hr className="v-sep" />

      <span>Scale: {value.a.toFixed(3)}</span>
    </div>
  );

  return (
    /* outermost div is just a wrapper so we can access the width in terms of pixels (via a ref) for the inner component, probably a better way to do this */
    <div ref={containerRef} className={styles.container}>
      <Toolbar />

      {/* require container div to restrict autosizer */}
      <div style={{ flex: 1 }}>
        <AutoSizer>
          {({ width, height }) => (
            <ReactSVGPanZoom
              ref={panzoomRef}
              tool={svgState.tool}
              onChangeTool={tool => setSvgState({ ...svgState, tool })}
              value={value}
              scaleFactorMin={0.01}
              scaleFactorMax={3}
              onChangeValue={compose(dispatch, reduxSetValue)}
              SVGBackground="#333"
              background="#444"
              width={width}
              customToolbar={() => <GridToolbar panzoomRef={panzoomRef} />}
              height={height}
            >
              <svg width={gridWidth} height={gridHeight} xmlns="http://www.w3.org/2000/svg">
                <DrawingPad
                  drawTool={drawTool}
                  navTool={svgState.tool}
                  scale={value.a}
                  ref={drawingPadRef}
                />
              </svg>
            </ReactSVGPanZoom>
          )}
        </AutoSizer>
      </div>
    </div>
  );
};

export default Layout;
