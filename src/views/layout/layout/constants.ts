export const ZOOM_FACTOR = 1.1;

// metre : pixel ratio. i.e. 1 unit -> GRAPH_SCALE px
// the small grid is 10 pixel
// for now, it makes sense to make this 10 such that one small cell is one unit
export const GRAPH_SCALE = 10;

/** the width of the largest line (and borders) */
export const GREEN_LINE_WIDTH = 5;

/**
 * this name is not possible to input so we can be sure it is unique
 * this is the name that the server assigns to the default management zone
 * */
export const DEFAULT_MZONE_NAME = "";
