import React, { forwardRef, useImperativeHandle, useRef, useState } from "react";
import { Tool as NavTool } from "react-svg-pan-zoom";
import { roundOff } from "../../../util/math";
import {
  dragVertex,
  toggleShapeSelection,
  transposeShape,
  toggleSelectedSensor,
  dragSensor,
  deleteSensorAsync,
  deleteSZoneAsync,
  addSZoneAsync,
  addSensorAsync,
  recalculateSensorZones,
} from "../actions";
import { Coordinate, SZone, Sensor } from "../types";
import { GRAPH_SCALE, GREEN_LINE_WIDTH, DEFAULT_MZONE_NAME } from "./constants";
import styles from "./drawing-pad.module.scss";
import { DrawTool } from "./Layout";
import { getNumericIdFromPrompt } from "../../../util/validation";
import { pointInPolygon } from "../../../util/geometry";
import { useGrid } from "../../../hooks";
import { useDispatch } from "react-redux";

type Props = {
  navTool: NavTool;
  drawTool: DrawTool;
  scale: number;
};

type Handles = {
  cancelDrawing: () => void;
  deleteSelected: () => void;
};

const DrawingPad: React.ForwardRefRenderFunction<Handles, Props> = (
  { drawTool, scale, navTool },
  ref
) => {
  const grid = useGrid();
  const dispatch = useDispatch();
  const { mzones, szones, selected, sensors, selectedSensor } = grid;
  const { minx, miny, maxx, maxy } = grid.rect;
  const [gridWidth, gridHeight] = [maxx - minx, maxy - miny];

  const cancelDrawing = () => {
    setIsDrawing(false);
    setCurrentPoly([]);
    setCurrentPoint(undefined);
  };

  const deleteSelected = () => {
    /** delete sensor first to assist with sql foreign key constraints */
    selectedSensor && deleteSensorAsync(dispatch)(selectedSensor);
    selected && deleteSZoneAsync(dispatch)(selected);
  };

  const svgRef = useRef<SVGSVGElement>(null);

  const handleShapeClicked = (shape: SZone) => dispatch(toggleShapeSelection(shape));

  const [currentPoint, setCurrentPoint] = useState<Coordinate>();
  const [currentPoly, setCurrentPoly] = useState<Coordinate[]>([]);
  const [isDrawing, setIsDrawing] = useState(false);
  const [initialDrag, setInitialDrag] = useState<Coordinate>();
  const [isDragging, setIsDragging] = useState(false);
  const [isSensorDragging, setIsSensorDragging] = useState(false);
  const [vertexIndex, setVertexIndex] = useState<number>(); // index of the vertex being dragged

  // translates the relative coordinates into the absolute
  const translateX = (x: number): number => x - minx;
  const translateY = (y: number): number => y - miny;
  const untranslateX = (x: number): number => x + minx;
  const untranslateY = (y: number): number => y + miny;

  useImperativeHandle(ref, () => ({
    cancelDrawing,
    deleteSelected,
  }));

  /**
   * only register clicks during pan; we don't want selection during zoom;
   * we also can't have pointer events when drawing otherwise mouse events get blocked by the shape currently drawing and we can't stop drawing
   * */
  const shapePointerEvents = navTool === "pan" ? "auto" : "none";

  /** call this is both the element mouse event as well as the surrounding in case the mouse moves off the element */
  const handleShapeDrag = (e: React.MouseEvent<SVGElement>) => {
    // don't drag when either not dragging or a vertex is currently getting dragged
    if (!isDragging || vertexIndex) return;
    const [initx, inity] = initialDrag!;
    const [x, y] = getRelativeRoundedCoordinates(e);
    const [dx, dy] = [x - initx, y - inity];
    setInitialDrag(getRelativeRoundedCoordinates(e));
    if (!selected) return;
    dispatch(transposeShape(selected.id, dx, dy));
  };

  const handleVertexDrag = (e: React.MouseEvent<SVGElement>) => {
    // explicit undefined eq check as index 0 is considered falsey
    if (!selected || vertexIndex === undefined) return;
    const [x, y] = getRelativeRoundedCoordinates(e);
    dispatch(dragVertex(selected.id, vertexIndex, untranslateX(x), untranslateY(y)));
  };

  const handleSensorDrag = (e: React.MouseEvent<SVGElement>) => {
    if (!selectedSensor || !isSensorDragging) return;
    const [cx, cy] = getSensorCoordinates(e);
    dispatch(dragSensor(selectedSensor.id, untranslateX(cx), untranslateY(cy)));
  };

  const genericOnMouseUp = () => {
    // if a vertex/shape has been moved or the sensor dragged, the sensor szone may now be invalid
    if (vertexIndex !== undefined || isSensorDragging || isDragging)
      dispatch(recalculateSensorZones());
    setVertexIndex(undefined);
    setIsSensorDragging(false);
    setIsDragging(false);
  };

  const genericOnMouseMove = (e: React.MouseEvent<SVGElement>) => {
    handleVertexDrag(e);
    handleShapeDrag(e);
    handleSensorDrag(e);
  };

  const renderShape = (shape: SZone) => {
    const className = selected === shape ? styles["selected-shape"] : styles.shape;
    const style: React.CSSProperties = {
      pointerEvents: shapePointerEvents,
      strokeWidth: `${2 / scale}px`,
    };

    const handlers: React.SVGAttributes<SVGElement> = {
      onDoubleClick: () => handleShapeClicked(shape),
      // onTouchEnd: e => handleShapeClicked(e, shape),
      onMouseDown: e => {
        if (selected !== shape) return;
        setInitialDrag(getRelativeRoundedCoordinates(e));
        e.stopPropagation(); // stop the pan from occuring
        setIsDragging(true);
      },
      // the mouse move is registered on both the shape as well as the svg so it continues to work if the mouse leaves the shape
      onMouseMove: genericOnMouseMove,
      onMouseUp: genericOnMouseUp,
    };

    switch (shape.kind) {
      case "polygon": {
        const { id, points } = shape;
        return (
          <React.Fragment key={id}>
            <polygon
              className={className}
              style={style}
              key={id}
              points={points.map(([x, y]) => [translateX(x), translateY(y)]).join(" ")}
              {...handlers}
            />
            {/* ensure to draw the handles after all shapes are drawn for correct layering */}
            {selected === shape &&
              points.map(([x, y], i) => (
                <circle
                  key={i}
                  className={styles.draghandle}
                  cx={translateX(x)}
                  cy={translateY(y)}
                  r={8 / scale}
                  fill="white"
                  onMouseDown={e => {
                    e.stopPropagation();
                    setVertexIndex(i);
                  }}
                  onMouseMove={handleVertexDrag}
                  onMouseUp={genericOnMouseUp}
                />
              ))}
          </React.Fragment>
        );
      }
    }
  };

  const drawSensor = (sensor: Sensor) => {
    const [x, y] = [translateX(sensor.cx), translateY(sensor.cy)];
    return (
      <circle
        className={styles.sensor}
        onDoubleClick={() => dispatch(toggleSelectedSensor(sensor))}
        onMouseDown={e => {
          if (sensor !== selectedSensor) return;
          e.stopPropagation();
          setIsSensorDragging(true);
        }}
        onMouseUp={genericOnMouseUp}
        onMouseMove={handleSensorDrag}
        key={sensor.id}
        cx={x}
        cy={y}
        r={GRAPH_SCALE / 3}
        fill={sensor.id === selectedSensor?.id ? "lightgreen" : "white"}
      />
    );
  };

  const drawCircle = ([x, y]: Coordinate) => (
    <circle className={styles.circle} cx={x} cy={y} r={7 / scale} onMouseUp={finalizeDrawing} />
  );

  const drawPartialPoly = (points: Coordinate[]) => {
    const scaledPoints = points.map(([x, y]) => [x, y]).join(" ");
    return (
      <polyline
        points={scaledPoints}
        pointerEvents={shapePointerEvents}
        stroke="orange"
        strokeWidth={1 / scale}
        fill="none"
      />
    );
  };

  const getRelativeCoordinates = (e: React.MouseEvent): Coordinate => {
    const rect = svgRef.current!.getBoundingClientRect();
    const x = e.pageX - rect.x;
    const y = e.pageY - rect.y;
    return [x / scale, y / scale];
  };

  /** returns the x y coordinates relative to the grid (snapped to nearest grid unit)*/
  const getRelativeRoundedCoordinates = (e: React.MouseEvent): Coordinate => {
    const [x, y] = getRelativeCoordinates(e);
    const roundedX = roundOff(x, GRAPH_SCALE);
    const roundedY = roundOff(y, GRAPH_SCALE);
    return [roundedX, roundedY];
  };

  const getSensorCoordinates = (e: React.MouseEvent): Coordinate => {
    const [rawx, rawy] = getRelativeCoordinates(e);
    // round down (towards top left) and then add the GRAPH_SCALE / 2 to find the center
    return [
      10 * Math.floor(rawx / 10) + GRAPH_SCALE / 2,
      10 * Math.floor(rawy / 10) + GRAPH_SCALE / 2,
    ];
  };

  const finalizeDrawing = async () => {
    // modifying the shape such that the last point is removed as polygon will connect the last first and last points
    const [, ...xs] = currentPoly;
    if (xs.length < 3) return;

    // TODO get a nicer mechanism  for id / add validation / check uniqueness of id
    setIsDrawing(false);

    const id = getNumericIdFromPrompt(
      x => szones.map(s => s.id).includes(x),
      "Please Enter ID For New Zone"
    );
    if (!id) return;

    /* const mzoneId = promptMZoneName(mzones);
     * if (mzoneId === null) return;
     * if (typeof mzoneId === "string")
     *     return toast.error(`Management zone with name \`${mzoneId}\` not found`); */

    addSZoneAsync(dispatch)(
      grid.id,
      new SZone(
        id,
        mzones.find(mzone => mzone.name === DEFAULT_MZONE_NAME)!.id,
        xs.map(([x, y]) => [untranslateX(x), untranslateY(y)])
      )
    );
    setCurrentPoly([]);
    setCurrentPoint(undefined);
  };

  return (
    <svg
      width={gridWidth}
      height={gridHeight}
      className={styles["svg-container"]}
      xmlns="http://www.w3.org/2000/svg"
      ref={svgRef}
    >
      <defs>
        <pattern id="sml-grid" width={10} height={10} patternUnits="userSpaceOnUse">
          <path
            d="M 10 0 L 0 0 0 10"
            fill="none"
            stroke="#ffffff55"
            strokeWidth={1}
            shapeRendering="crispEdges"
          />
        </pattern>
        <pattern id="med-grid" width={100} height={100} patternUnits="userSpaceOnUse">
          <rect width={100} height={100} fill="url(#sml-grid)" />
          <path d="M 100 0 L 0 0 0 100" fill="none" stroke="#00bfffaa" strokeWidth={3} />
        </pattern>
        <pattern id="lrg-grid" width={1000} height={1000} patternUnits="userSpaceOnUse">
          <rect width={1000} height={1000} fill="url(#med-grid)" />
          <path
            d="M 1000 0 L 0 0 0 1000"
            fill="none"
            stroke="green"
            strokeWidth={GREEN_LINE_WIDTH}
          />
        </pattern>
      </defs>

      <rect
        width={gridWidth}
        height={gridHeight}
        fill="url(#lrg-grid)"
        onDoubleClick={() => {
          selected && handleShapeClicked(selected); // deselect on grid click
          selectedSensor && dispatch(toggleSelectedSensor(selectedSensor));
        }}
        onMouseDown={e => {
          switch (drawTool) {
            case "poly": {
              const coords = getRelativeRoundedCoordinates(e);
              setIsDrawing(true);
              // start by a line with start == end
              if (currentPoly.length === 0) {
                setCurrentPoly([coords, coords]);
                // use the point as the way of ending drawing
                setCurrentPoint(getRelativeRoundedCoordinates(e));
              } else setCurrentPoly([coords, ...currentPoly]);
            }
          }
        }}
        onMouseMove={e => {
          genericOnMouseMove(e);
          if (isDrawing && drawTool === "poly") {
            // replace last drawn point with the current mouse pos
            const [, ...xs] = currentPoly;
            setCurrentPoly([getRelativeRoundedCoordinates(e), ...xs]);
          }
        }}
        onClick={e => {
          switch (drawTool) {
            case "sensor": {
              const [cx, cy] = getSensorCoordinates(e);
              const [ucx, ucy] = [untranslateX(cx), untranslateY(cy)];
              const containedIn = szones.find(shape => pointInPolygon([ucx, ucy], shape.points));
              const id = getNumericIdFromPrompt(
                id => sensors.find(s => s.id === id) !== undefined,
                "Please Enter ID For New Sensor"
              );
              if (id === null) return;
              const sensor = new Sensor(id, "V", ucx, ucy, grid.id, containedIn?.id);
              console.log(sensor);
              addSensorAsync(dispatch)(sensor);
            }
          }
        }}
        onMouseUp={genericOnMouseUp}
      />
      {/* lines acting as bottom & right borders; noticeably absent when graph is not a multiple of 1000 */}
      <line
        x1={0}
        x2={gridWidth}
        y1={gridHeight}
        y2={gridHeight}
        stroke="green"
        strokeWidth={GREEN_LINE_WIDTH}
      />
      <line
        x1={gridWidth}
        x2={gridWidth}
        y1={0}
        y2={gridHeight}
        stroke="green"
        strokeWidth={GREEN_LINE_WIDTH}
      />

      {szones.filter(s => s.id !== selected?.id).map(renderShape)}
      {/* rendered selected shape last so it is always on top of other shapes*/}
      {selected && renderShape(selected)}

      {currentPoint && drawCircle(currentPoint)}
      {sensors.map(drawSensor)}
      {/* what you are currently drawing should take maximum precedence */}
      {drawPartialPoly(currentPoly)}
    </svg>
  );
};

export default forwardRef(DrawingPad);
