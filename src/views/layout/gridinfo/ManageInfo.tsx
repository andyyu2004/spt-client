import React from "react";
import ManagementZoneSummary from "./ManagementZoneSummary";
import MZoneDetails from "./MZoneDetails";
import { useGrid } from "../../../hooks";

const ManageInfo: React.FC = () => {
  const grid = useGrid();
  const { selectedMZone } = grid;

  return (
    <div>
      <ManagementZoneSummary />
      {selectedMZone && <MZoneDetails />}
    </div>
  );
};

export default ManageInfo;
