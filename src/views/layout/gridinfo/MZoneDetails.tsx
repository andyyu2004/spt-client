import assert from "assert";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { setPopupContent, closePopup } from "../../../redux/reducers/popup";
import {
  addRuleAsync,
  deleteRuleAsync,
  reorderRules,
  updateRuleAsync,
  setEditMZone,
} from "../actions";
import { InitRuleInfo, Rule } from "../../../components/rules/types";
import { DraggableList } from "../../../components";
import { RuleForm } from "../../../components/rules";
import { RuleFormKind } from "../../../components/rules";
import { useGrid } from "../../../hooks";

const MZoneDetails: React.FC = () => {
  const grid = useGrid();
  const { selectedMZone, rules, editMZoneId } = grid;
  const mzone = selectedMZone!;
  assert(mzone !== undefined);

  const dispatch = useDispatch();

  const RuleSelect: React.FC = () => {
    const [selectedRuleKind, setSelectedRuleKind] = useState<RuleFormKind>(RuleFormKind.Time);

    const onSubmitRule = (initInfo: InitRuleInfo) => {
      addRuleAsync(dispatch)(initInfo);
      dispatch(closePopup());
    };

    return (
      <div className="v-flex-container">
        <h3>Select Rule Type</h3>
        <select
          style={{ width: "100px" }}
          name="rule-select"
          onChange={e => setSelectedRuleKind(e.target.value as RuleFormKind)}
        >
          {Object.values(RuleFormKind).map(rule => (
            <option key={rule}>{rule}</option>
          ))}
        </select>

        <RuleForm grid={grid} onSubmit={onSubmitRule} rulekind={selectedRuleKind} />
      </div>
    );
  };

  const handleEditRule = (rule: Rule) => {
    const handleUpdateRule = (rule: Rule) => {
      updateRuleAsync(dispatch)(rule);
      dispatch(closePopup());
    };

    switch (rule.kind.type) {
      case "Threshold":
        return (
          <RuleForm
            grid={grid}
            onUpdate={handleUpdateRule}
            initRule={rule}
            rulekind={RuleFormKind.Threshold}
          />
        );
      case "Time":
        return (
          <RuleForm
            grid={grid}
            onUpdate={handleUpdateRule}
            initRule={rule}
            rulekind={RuleFormKind.Time}
          />
        );
      default:
        return <div>Unknown rule type</div>;
    }
  };

  return (
    <div>
      <h3 className="inline-section-title">{mzone.name}</h3>
      <button
        style={{ color: editMZoneId ? "lightgreen" : "white" }}
        onClick={() => dispatch(setEditMZone(mzone.id))}
      >
        Edit Zone
      </button>
      <br />
      <h4 className="inline-section-title">Rules:</h4>
      <button onClick={() => dispatch(setPopupContent(<RuleSelect />))}>+</button>
      <DraggableList
        rules={rules.filter(r => r.mzone_id === mzone.id)}
        droppableId={mzone.id}
        update={rules => dispatch(reorderRules(rules))}
        editRule={rule => dispatch(setPopupContent(handleEditRule(rule)))}
        deleteRule={deleteRuleAsync(dispatch)}
      />
    </div>
  );
};

export default MZoneDetails;
