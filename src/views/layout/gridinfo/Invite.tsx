import React from "react";
import { useState } from "react";
import { apiGenerateInviteLink } from "../../../api";
import { setPopupContent } from "../../../redux/reducers/popup";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";
import { RouteComponentProps } from "@reach/router";

/**
 * possible permissions for a user on a particular grid
 * Admin => Write => Read
 */
export enum GridPermissions {
  Read = 1 << 0,
  Write = 1 << 1,
  Admin = 1 << 2,
}

type Props = { gridId: number } & RouteComponentProps;

const Invite: React.FC<Props> = ({ gridId }) => {
  const [permissions, setPermissions] = useState();

  const handleChange = (e: any) => setPermissions(e.target.value);
  const rdispatch = useDispatch();

  const handleSubmit = async (e: any) => {
    e.preventDefault();

    const Link: React.FC<{ link: string }> = ({ link }) => (
      <>
        <h4>Invitation Link</h4>
        <input type="text" readOnly value={link} />
      </>
    );

    // permission * 2 - 1 sets all the permissions below it
    // i.e. admin will also implicitly set read and write permissions
    (await apiGenerateInviteLink(gridId, permissions! * 2 - 1))
      .map(link => rdispatch(setPopupContent(<Link link={link} />)))
      .mapLeft(toast.error);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div onChange={handleChange}>
        <h3>Invitation Permissions</h3>
        <label>
          <input required type="radio" name="permissions" value={GridPermissions.Read} />
          Read
        </label>
        <label>
          <input required type="radio" name="permissions" value={GridPermissions.Write} />
          Write
        </label>
        <label>
          <input required type="radio" name="permissions" value={GridPermissions.Admin} />
          Admin
        </label>
      </div>
      <input type="submit" />
    </form>
  );
};

export default Invite;
