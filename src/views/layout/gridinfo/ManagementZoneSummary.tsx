import React, { useState, useEffect } from "react";
import { MZone } from "../types";
import Popup from "reactjs-popup";
import { SliderPicker } from "react-color";
import { updateMZoneAsync, toggleSelectedMZone, deleteMZoneAsync } from "../actions";
import styles from "./grid-info.module.scss";
import { NewMZoneForm } from "../../../components/forms";
import { setPopupContent } from "../../../redux/reducers/popup";
import { useDispatch } from "react-redux";
import { DEFAULT_MZONE_NAME } from "../layout/constants";
import { useGrid } from "../../../hooks";

type Props = {
  tmpColours: { [key: number]: string };
  setTmpColours: React.Dispatch<React.SetStateAction<{ [key: number]: string }>>;
  mzone: MZone;
};

/** Represents an entry in the Management Zone Summary Section */
const ManagementZoneSummaryEntry: React.FC<Props> = ({ mzone, tmpColours, setTmpColours }) => {
  const grid = useGrid();
  const dispatch = useDispatch();
  const { selectedMZone } = grid;

  return (
    <li key={mzone.id}>
      <Popup
        trigger={
          <div
            className={styles.trigger}
            style={{
              backgroundColor: tmpColours[mzone.id],
            }}
          />
        }
      >
        <SliderPicker
          color={tmpColours[mzone.id]}
          onChange={({ hex }) => setTmpColours({ ...tmpColours, [mzone.id]: hex })}
          onChangeComplete={({ hex }) => updateMZoneAsync(dispatch)({ ...mzone, colour: hex })}
        />
      </Popup>

      <button
        style={{
          color: mzone.id === selectedMZone?.id ? "lightgreen" : "white",
        }}
        onClick={() => dispatch(toggleSelectedMZone(mzone))}
      >
        {mzone.name} ({mzone.id})
      </button>
      <button className="x-button" onClick={() => deleteMZoneAsync(dispatch)(mzone)}>
        X
      </button>
    </li>
  );
};

const ManagementZoneSummary: React.FC = () => {
  const grid = useGrid();
  const { mzones } = grid;
  const [tmpColours, setTmpColours] = useState<{ [key: number]: string }>({});

  const rdispatch = useDispatch();

  /** set initial colours in state */
  useEffect(
    () => setTmpColours(mzones.reduce((acc, mzone) => ({ ...acc, [mzone.id]: mzone.colour }), {})),
    [mzones]
  );

  return (
    <div>
      <h3 className="inline-section-title">Management Zones</h3>
      <span className={styles["section-taskbar"]}>
        <button onClick={() => rdispatch(setPopupContent(<NewMZoneForm />))}>+</button>
      </span>

      <ul className={styles.list}>
        {mzones
          .filter(mzone => mzone.name !== DEFAULT_MZONE_NAME)
          .map(mzone => (
            <ManagementZoneSummaryEntry
              key={mzone.id}
              mzone={mzone}
              tmpColours={tmpColours}
              setTmpColours={setTmpColours}
            />
          ))}
      </ul>
    </div>
  );
};

export default ManagementZoneSummary;
