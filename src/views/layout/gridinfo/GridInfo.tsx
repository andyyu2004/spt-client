import React from "react";
import { LayoutInfo, ManageInfo, GridPermissions } from ".";
import { Toggle } from "../../../components";
import { DesignMode } from "../designer/Designer";
import styles from "./grid-info.module.scss";
import { navigate } from "@reach/router";
import { useGrid, usePermissions } from "../../../hooks";

type Props = {
  designMode: DesignMode;
  setDesignMode: React.Dispatch<React.SetStateAction<DesignMode>>;
};

const GridInfo: React.FC<Props> = ({ setDesignMode, designMode }) => {
  const grid = useGrid();
  const { name } = grid;
  const permissions = usePermissions(grid.id);

  return (
    <div className={styles.container}>
      <h3 className="inline-block">{name}</h3>
      {permissions & GridPermissions.Admin && (
        <button onClick={() => navigate(`/sites/${grid.id}/admin/`)}>Admin</button>
      )}
      <Toggle
        className={styles.toggle}
        onClick={() => setDesignMode(designMode === "layout" ? "manage" : "layout")}
        options={["Manage", "Layout"]}
        initialIndex={designMode === "layout" ? 1 : 0}
      />
      <br />
      {designMode === "manage" ? <ManageInfo /> : <LayoutInfo />}
    </div>
  );
};

export default GridInfo;
