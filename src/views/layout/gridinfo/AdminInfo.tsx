import React from "react";
import styles from "./grid-info.module.scss";
import { useDispatch } from "react-redux";
import { setPopupContent } from "../../../redux/reducers/popup";
import { Invite } from ".";
import { GridAction } from "../actions";
import { Grid } from "../types";

type Props = {
  grid: Grid;
  dispatch: React.Dispatch<GridAction>;
};

const AdminInfo: React.FC<Props> = ({ grid, dispatch }) => {
  const rdispatch = useDispatch();
  const handleInvite = () => rdispatch(setPopupContent(<Invite gridId={grid.id} />));
  return (
    <div className={styles.container}>
      <button onClick={handleInvite}>Invite</button>
    </div>
  );
};

export default AdminInfo;
