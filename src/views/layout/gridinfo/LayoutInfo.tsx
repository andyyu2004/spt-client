import React from "react";
import { GridSizeForm } from "../../../components/rules/forms";
import { deleteSZoneAsync, deleteSensorAsync } from "../actions";
import { setPopupContent } from "../../../redux/reducers/popup";
import { useDispatch } from "react-redux";
import { useGrid } from "../../../hooks";

/** Layout part of GridInfo */
const LayoutInfo: React.FC = () => {
  const grid = useGrid();
  const { selected, selectedSensor } = grid;
  const actions = ["Select An Action", "Set Grid Size"];
  const dispatch = useDispatch();

  const handleActionSelected = (e: React.ChangeEvent<HTMLSelectElement>) => {
    switch (e.target.value) {
      case "Set Grid Size":
        dispatch(setPopupContent(<GridSizeForm grid={grid} />));
        break;
    }
  };

  return (
    <>
      {/* Layout Grid Info */}
      <form>
        <select value={"Select An Action"} onChange={handleActionSelected}>
          {actions.map(value => (
            <option key={value}>{value}</option>
          ))}
        </select>
      </form>
      <br />

      {selected && (
        <>
          <span>Irrigation Zone: #{selected.id}</span>
          <button className="x-button" onClick={() => deleteSZoneAsync(dispatch)(selected)}>
            Delete Zone
          </button>
        </>
      )}

      {selectedSensor && (
        <>
          <span>Sensor: #{selectedSensor.id}</span>
          <span>In irrigation zone: {selectedSensor.szone_id ?? "None"}</span>
          <button className="x-button" onClick={() => deleteSensorAsync(dispatch)(selectedSensor)}>
            Delete Sensor
          </button>
        </>
      )}
    </>
  );
};

export default LayoutInfo;
