import { Rule } from "../../components/rules/types";

import { GRAPH_SCALE } from "./layout/constants";

export type Rect = {
  minx: number;
  miny: number;
  maxx: number;
  maxy: number;
};

/** top-level state for rendering */
export class Grid {
  // these fields are local state and are not persisted in the database
  public selected?: SZone = undefined;
  public selectedSensor?: Sensor = undefined;
  public selectedMZone?: MZone = undefined;
  /** the mzone currently being edited */
  public editMZoneId?: number = undefined;

  constructor(
    public id: number,
    public name: string,
    /** bounding coordinates */
    public rect: Rect,
    /** solenoid zones */
    public szones: SZone[],
    /** management zones */
    public mzones: MZone[],
    public sensors: Sensor[],
    public rules: Rule[]
  ) {}

  // reconstructs a grid object from the json representation
  static fromJson(grid: Grid): Grid {
    const { id, name, rect, mzones, rules, sensors, szones } = grid;
    return new Grid(
      id,
      name,
      rect,
      szones.map(SZone.fromJson),
      mzones.map(MZone.fromJson),
      sensors.map(Sensor.fromJson),
      rules.map(Rule.fromJson)
    );
  }

  static defaultWithId(id: number): Grid {
    return new Grid(
      id,
      "Default Grid",
      { minx: -500, maxx: 500, miny: -500, maxy: 500 },
      [],
      [],
      [],
      []
    );
  }

  static default(): Grid {
    return Grid.defaultWithId(-1);
  }

  withinBound = function (this: Grid, rect: Rect): boolean {
    return (
      this.szones.every(s => s.withinBound(rect)) && this.sensors.every(s => s.withinBound(rect))
    );
  };

  // This must be defined as a function, not an arrow or a method
  // https://stackoverflow.com/questions/59657375/strange-javascript-tostring-behaviour
  // toString = function(this: Grid) { return `Grid { (${this.dimension}),
  // [${this.shapes}] }` }
}

/** (width, height) */
export type Dimension = [number, number];
/** (x, y) */
export type Coordinate = [number, number];

export class MZone {
  constructor(
    public readonly id: number,
    public name: string,
    public grid_id: number,
    public withhold_until: number,
    public colour: string
  ) {}

  static fromJson({ id, name, grid_id, withhold_until, colour }: MZone): MZone {
    return new MZone(id, name, grid_id, withhold_until, colour);
  }
}

// note: declaring `kind` as readonly in each implementation is required for
// discrimination to typecheck
interface IShape {
  readonly kind: string;
  transpose(dx: number, dy: number): IShape;
  transposeInPlace(dx: number, dy: number): void;
  /// returns the vertices of the shape in units (not pixels)
  vertices(): Coordinate[];
  withinBound(rect: Rect): boolean;
}

export class SZone implements IShape {
  readonly kind = "polygon";

  constructor(public readonly id: number, public mzone_id: number, public points: Coordinate[]) {}

  public static fromJson({ id, mzone_id, points }: SZone): SZone {
    return new SZone(id, mzone_id, points);
  }

  vertices() {
    return this.points.map<Coordinate>(([x, y]) => [x / GRAPH_SCALE, y / GRAPH_SCALE]);
  }

  transpose(dx: number, dy: number): SZone {
    return new SZone(
      this.id,
      this.mzone_id,
      this.points.map(([x, y]) => [x + dx, y + dy])
    );
  }

  transposeInPlace(dx: number, dy: number) {
    this.points = this.points.map(([x, y]) => [x + dx, y + dy]);
  }

  withinBound(rect: Rect): boolean {
    const { minx, miny, maxx, maxy } = rect;
    return this.points.every(([x, y]) => x >= minx && x <= maxx && y >= miny && y <= maxy);
  }
}

export class Sensor {
  // Volumetric and Matric sensors
  constructor(
    public readonly id: number,
    public readonly kind: "M" | "V",
    public cx: number,
    public cy: number,
    public grid_id: number,
    public szone_id?: number,
    public low: number = 415,
    public high: number = 1020
  ) {}

  withinBound(rect: Rect): boolean {
    const r = GRAPH_SCALE / 2;
    const [minx, maxx, miny, maxy] = [this.cx - r, this.cx + r, this.cy - r, this.cy + r];

    return minx >= rect.minx && maxx <= rect.maxx && miny >= rect.miny && maxy <= rect.maxy;
  }

  public static fromJson(sensor: Sensor): Sensor {
    const { id, szone_id, grid_id, kind, cx, cy, low, high } = sensor;
    return new Sensor(id, kind, cx, cy, grid_id, szone_id, low, high);
  }
}

export class Reading {
  constructor(
    public id: number,
    public sensor_id: number,
    public ctime: number,
    public moisture: number
  ) {}

  public static fromJson(reading: Reading): Reading {
    const { id, sensor_id, ctime, moisture } = reading;
    return new Reading(id, sensor_id, ctime, moisture);
  }
}
