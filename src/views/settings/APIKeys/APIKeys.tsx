import React, { useState, useEffect } from "react";
import { RouteComponentProps } from "@reach/router";
import { toast } from "react-toastify";
import styles from "./apikeys.module.scss";
import { ApiKey } from "../../../api/types";
import { fetchApiKeys } from "../../../api";

const APIKeys: React.FC<RouteComponentProps> = () => {
  const [keys, setKeys] = useState<ApiKey[]>([]);

  const fetchKeys = async () => (await fetchApiKeys()).map(setKeys).mapLeft(toast.error);

  useEffect(() => {
    fetchKeys();
  }, []);

  const handleCopy = (key: string) =>
    navigator?.clipboard
      ?.writeText(key)
      ?.then(() => toast.success("Copied Key To Clipboard"))
      .catch(toast.error);

  return (
    <div className={styles.container}>
      <h3 className={styles.headings}>API Keys</h3>
      {keys.length !== 0 && (
        <ul>
          {keys?.map(key => (
            <li key={key.hash.toString()}>
              <button className={styles.keys} onClick={() => handleCopy(key.apikey)}>
                {key.grid_name} - {key.apikey}
              </button>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default APIKeys;
