import React from "react";
import { RouteComponentProps, Router, navigate } from "@reach/router";
import APIKeys from "./APIKeys";
import { Sidebar } from "../../components";
import styles from "./settings.module.scss";

const sidebarItems: [string, () => void][] = [["API Keys", () => navigate("apikeys")]];

const Settings: React.FC<RouteComponentProps> = () => {
  return (
    <div className={styles.container}>
      <Sidebar title="Settings" items={sidebarItems} />
      <Router className={styles.inner}>
        <APIKeys path="/apikeys" />
      </Router>
    </div>
  );
};

export default Settings;
