import assert from 'assert';

// An Elegant Pairing Function by Matthew Szudzik @ Wolfram Research
export const pair = ([x, y]: [number, number]) : number => {
    assert(x >= 0 && y >= 0, "Pairing function must take non-negative numbers");
    return (x >= y) ? (x * x + x + y) : (y * y + x);
} 
    
export function unpair(x: number): [number, number] {
    const sqrtz = Math.floor(Math.sqrt(x));
    const sqz = sqrtz * sqrtz;
    return ((x - sqz) >= sqrtz) ? [sqrtz, x - sqz - sqrtz] : [x - sqz, sqrtz];
}