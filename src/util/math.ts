
/** round a number to the nearest interval */
export const roundOff = (i: number, to: number) : number => Math.round(i / to) * to;

