import { Coordinate } from "../views/layout/types";
import { GRAPH_SCALE } from "../views/layout/layout/constants";

export function parsePoints(poly: string): Coordinate[] {
  const pairs: string[] = poly.trimEnd().split(" ");
  const points: Coordinate[] = pairs.map(p => {
    const [x, y] = p.split(",");
    return [parseInt(x) * GRAPH_SCALE, parseInt(y) * GRAPH_SCALE];
  });

  // Ensure first and last points join up to form a valid svg polygon
  if (points[0] !== points[points.length - 1]) {
    points.push(points[0]);
  }
  return points;
}
