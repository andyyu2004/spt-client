import { Coordinate } from "../views/layout/types";

// The MIT License (MIT)
// Copyright (c) 2016 James Halliday

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

export function pointInPolygon(point: Coordinate, ps: Coordinate[]) {
  // ray-casting algorithm based on
  // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

  const [x, y] = point;

  let inside = false;
  for (let i = 0, j = ps.length - 1; i < ps.length; j = i++) {
    const xi = ps[i][0],
      yi = ps[i][1];
    const xj = ps[j][0],
      yj = ps[j][1];

    /* eslint-disable no-mixed-operators */
    const intersect = yi > y !== yj > y && x < ((xj - xi) * (y - yi)) / (yj - yi) + xi;
    if (intersect) inside = !inside;
  }

  return inside;
}
