import assert from "assert";
import { Moment } from "moment";
import moment from "moment";
import { Day, dayToInt } from "../components/rules/types";
import { referenceDay } from "../components/rules/forms/threshold";

export class Time {
  private _hour: number;
  private _minute: number;
  private _second: number;
  /** flag variable to ensure toUTC() is idempotent */
  private isUTC: boolean = false;

  public get second(): number {
    return this._second;
  }

  public get hour(): number {
    return this._hour;
  }

  public get minute(): number {
    return this._minute;
  }

  // multiplying by shift just to make sure the hour is large enough to not get negative modulos
  public shiftHour(shift: number) {
    this._hour = (this._hour + shift + 24 * Math.abs(shift)) % 24;
  }

  public toJSON(): string {
    return this.toString();
  }

  /// converts self(time) and a day to a moment, relative to `referenceDay`
  public toMoment(day: Day): Moment {
    return moment({
      ...referenceDay,
      day: dayToInt(day),
      hour: this.hour,
      minute: this.minute,
      second: this._second,
    });
  }

  public toUTC() {
    // note that addition is indeed the correct operation here
    // suppose we are in +12NZT, then getTimezoneOffset returns -720
    if (this.isUTC) return;
    this.shiftMinute(new Date().getTimezoneOffset());
    this.isUTC = true;
  }

  public fromUTC() {
    if (!this.isUTC) return;
    this.shiftMinute(-new Date().getTimezoneOffset());
    this.isUTC = false;
  }

  public toString(): string {
    const hour_str = this._hour.toLocaleString("en-US", {
      minimumIntegerDigits: 2,
      useGrouping: false,
    });
    const minute_str = this._minute.toLocaleString("en-US", {
      minimumIntegerDigits: 2,
      useGrouping: false,
    });
    const sec_str = this._second.toLocaleString("en-US", {
      minimumIntegerDigits: 2,
      useGrouping: false,
    });
    return `${hour_str}:${minute_str}:${sec_str}`;
  }

  /**
   * it thinks its a Time object but it's actually a string, so treat it as such.
   * also, assume that the JSON is in UTC and that we want it in local time
   */

  public static fromJson(t: Time): Time {
    const time = Time.fromStr((t as unknown) as string);
    // assume UTC
    time.isUTC = true;
    time.fromUTC();
    return time;
  }

  /// converts string of form HH:MM to a Time instance
  public static fromStr(s: string): Time {
    assert(/([0-1][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]/.test(s));
    const [hours, minute, seconds] = s.split(":");
    return new Time(parseInt(hours), parseInt(minute), parseInt(seconds));
  }

  public static fromMoment(m: Moment): Time {
    return new Time(m.hour(), m.minute(), m.second());
  }

  public shiftMinute(shift: number) {
    const newMinutes = this._minute + shift;
    const hours = Math.floor(newMinutes / 60);
    this._minute = (newMinutes + 60 * Math.abs(hours)) % 60;
    this.shiftHour(hours);
  }

  public constructor(hour: number, minute: number, second: number) {
    assert(hour >= 0 && hour < 24);
    assert(minute >= 0 && minute < 60);
    assert(second >= 0 && second < 60);
    this._hour = hour;
    this._minute = minute;
    this._second = second;
  }
}
