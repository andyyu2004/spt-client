import { roundOff } from '../math';

test("rounding", () => {
    expect(roundOff(110, 25)).toEqual(100);
    expect(roundOff(110, 20)).toEqual(120);
    expect(roundOff(95, 10)).toEqual(100);
    expect(roundOff(94, 10)).toEqual(90);
});