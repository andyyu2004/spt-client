import { pair, unpair } from "../pairing";

test("pairing-and-unpairing", () => {
    const p: [number, number] = [10, 24];
    const paired = pair(p);
    const unpaired = unpair(paired);
    expect(p).toEqual(unpaired);
});