import { Time } from "../time";
import { Day } from "../../components/rules/types";

test("init time", () => {
  expect(new Time(11, 30, 0).hour).toEqual(11);
  expect(new Time(11, 30, 0).minute).toEqual(30);
});

test("shift time simple", () => {
  const time = new Time(11, 30, 0);
  time.shiftHour(1);
  expect(time.hour).toEqual(12);
  expect(time.minute).toEqual(30);
});

test("shift time complex", () => {
  const time = new Time(11, 30, 0);
  time.shiftHour(-12);
  expect(time.hour).toEqual(23);
  expect(time.minute).toEqual(30);
  time.shiftMinute(35);
  expect(time.minute).toEqual(5);
  expect(time.hour).toEqual(0);
  time.shiftMinute(-6);
  expect(time.minute).toEqual(59);
  expect(time.hour).toEqual(23);
});

test("large negative shift", () => {
  const time = new Time(0, 0, 0);
  time.shiftMinute(-1445);
  expect(time.hour).toEqual(23);
  expect(time.minute).toEqual(55);
});

test("time to moment", () => {
  const time = new Time(3, 30, 45);
  const moment = time.toMoment(Day.Wed);
  expect(moment.hour()).toEqual(3);
  expect(moment.minute()).toEqual(30);
  expect(moment.second()).toEqual(45);
});

