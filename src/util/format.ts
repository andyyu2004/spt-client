import { SZone } from "../views/layout/types";
import { GRAPH_SCALE } from "../views/layout/layout/constants";

export function fmtSet<T>(set: Set<T>): string {
  return Array.from(set).join(", ");
}

export function fmtPair<T, U>([t, u]: [T, U]): string {
  return `(${t}, ${u})`;
}
export function fmtShape(shape: SZone): string {
  switch (shape.kind) {
    // case "rect": {
    //     const { x, y, width, height, id } = shape;
    //     return `Rect #${id} { ${width / GRAPH_SCALE} x ${height/ GRAPH_SCALE} at (${x / GRAPH_SCALE}, ${y / GRAPH_SCALE}) }`
    // }
    case "polygon": {
      const { points, id } = shape;
      return `Poly #${id} { ${points
        .map(([x, y]) => fmtPair([x / GRAPH_SCALE, y / GRAPH_SCALE]))
        .join(", ")} }`;
    }
  }
}
