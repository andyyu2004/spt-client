import { MZone } from "../views/layout/types";

/** takes a uniquenessTest; the test should return true if NOT unique; this test can be used more generally as a validation test not just for uniqueness */
export function getNumericIdFromPrompt(
  pred: (id: number) => boolean,
  promptMsg: string
): number | null {
  let input = prompt(promptMsg);
  while (true) {
    if (input === null) return null; // allow user to continue drawing on cancel

    if (!/^[0-9]+$/.test(input)) {
      // check is valid base 10 integer literal
      input = prompt("Invalid base 10 integer, please try again");
    } else if (pred(parseInt(input))) {
      input = prompt("ID already exists");
    } else return parseInt(input!);
  }
}

/// returns a valid mzone id or the name of the mzone if mzone was not found and null if nothing was entered :)
export function promptMZoneName(mzones: MZone[]): number | null {
  let input = prompt(
    "Please Enter the Name of the Management Zone the solenoid zone should be placed into"
  );
  while (true) {
    if (input === null) return null;
    const id = mzoneIdFromName(mzones, input);
    if (id) return id;
    input = prompt(`Management zone with name \`${input}\` not found`);
  }
}

export function mzoneIdFromName(mzones: MZone[], name: string): number | null {
  return mzones.find(m => m.name === name)?.id ?? null;
}

export function validateEmail(email: string) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
