export function range(until: number): number[] {
  return Array.from(Array(until).keys());
  // const xs = [];
  // for (let i = 0; i < until; i++) xs.push(i);
  // return xs;
}

/** return whether all elements in an iterable are unique */
export function allUnique<T>(xs: Iterable<T>): boolean {
  const set = new Set();
  for (const x of xs) {
    if (set.has(x)) return false;
    set.add(x);
  }
  return true;
}

export function fst<T, U>([x, _]: [T, U]): T {
  return x;
}
export function snd<T, U>([_, y]: [T, U]): U {
  return y;
}
