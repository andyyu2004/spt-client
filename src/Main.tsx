import { Router, navigate } from "@reach/router";
import React, { ReactElement } from "react";
import styles from "./main.module.scss";
import { APITest as Settings, Error, Home, Instances } from "./views";
import { Header } from "./components";
import { logoutCurrentUser } from "./redux/reducers/user";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "./api";
import { AppState } from "./redux/reducers";
import Popup from "reactjs-popup";
import { closePopup } from "./redux/reducers/popup";

const Main: React.FC = () => {
  const dispatch = useDispatch();

  const popupContent = useSelector<AppState, ReactElement | undefined>(
    state => state.popup.content
  );

  const handleLogout = () => {
    logout();
    dispatch(logoutCurrentUser());
    navigate("/");
  };

  // listen for warnings to all sites
  React.useEffect(() => {
    const source = new EventSource("http://localhost:3001/api/warnings");
    source.onmessage = e => console.log("main", e.data);
    return () => source.close();
  }, []);

  return (
    <div className={styles.container}>
      <Header
        title="Irrigation Control System"
        items={[
          // note the trailing slash in navigate is meaningful
          ["Sites", () => navigate("/sites/")],
          ["Settings", () => navigate("/settings/")],
          ["Logout", handleLogout],
        ]}
      />

      <Popup
        open={popupContent !== undefined}
        onClose={() => dispatch(closePopup())}
        // if needed, width and height can be specified below in `contentStyle`
        contentStyle={{ backgroundColor: "#111", border: "1px solid #777", padding: "0 4em" }}
      >
        <div>{popupContent}</div>
      </Popup>

      <Router className={styles.content}>
        <Home path="/" />
        <Instances path="/sites/*" />
        <Settings path="/settings/*/" />
        <Error path="*" code={404} />
      </Router>
    </div>
  );
};

export default Main;
