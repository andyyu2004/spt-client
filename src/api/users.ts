import axios from "axios";
import { apiErrorHandler } from ".";
import { Either, Right } from "../error/Either";
import { User } from "./types";

export const apiLogin = (
  email_or_username: string,
  password: string
): Promise<Either<string, User>> =>
  axios
    .post("/api/session", { email_or_username, password })
    .then<Either<string, User>>(({ data }) => new Right(data.user))
    .catch(apiErrorHandler);

export const logout = () => axios.delete("/api/session");

export const register = (
  email: string,
  username: string,
  password: string
): Promise<Either<string, User>> =>
  axios
    .post("/api/users", { email, username, password })
    .then<Either<string, User>>(({ data }) => new Right(data.user))
    .catch(apiErrorHandler);

export const checkAuth = (): Promise<Either<string, boolean>> =>
  axios
    .get("/api/auth")
    .then<Either<string, boolean>>(({ data: isAuth }) => new Right(isAuth))
    .catch(apiErrorHandler);
