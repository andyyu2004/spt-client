import { Either, Right } from "../error/Either";
import axios from "axios";
import { apiErrorHandler } from ".";
import { ApiKey } from "./types";

export async function requestApiKey(grid_name: string): Promise<Either<string, string>> {
  return axios
    .post("/api/apikeys", { grid_name })
    .then<Either<string, string>>(({ data }) => new Right(data.key))
    .catch(apiErrorHandler);
}

export async function fetchApiKeys(): Promise<Either<string, ApiKey[]>> {
  return axios
    .get("/api/apikeys")
    .then<Either<string, ApiKey[]>>(({ data }) => new Right(data.apikeys))
    .catch(apiErrorHandler);
}

export async function deleteApiKey(hash: Uint8Array): Promise<Either<string, []>> {
  return axios
    .delete("/api/apikeys", { data: { hash } })
    .then<Either<string, []>>(() => new Right([]))
    .catch(apiErrorHandler);
}

