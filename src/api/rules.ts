import axios from "axios";
import { apiErrorHandler } from ".";
import { Either, Right } from "../error/Either";
import { InitRuleInfo, Rule } from "../components/rules/types";

export async function apiAddRule(rule: InitRuleInfo): Promise<Either<string, Rule>> {
  return await axios
    .post(`/api/rules`, rule)
    .then<Either<string, Rule>>(({ data }) => new Right(Rule.fromJson(data.rule)))
    .catch(apiErrorHandler);
}

export async function apiUpdateRule(rule: Rule): Promise<Either<string, Rule>> {
  return await axios
    .put(`/api/rules`, rule)
    .then<Either<string, Rule>>(({ data }) => new Right(Rule.fromJson(data.rule)))
    .catch(apiErrorHandler);
}
