import axios from "axios";

import { Rule } from "../components/rules/types";
import { Either, Right } from "../error/Either";
import { UserPermissions } from "../redux/reducers/user";
import { Grid, MZone, Reading, Rect, Sensor, SZone } from "../views/layout/types";

import { apiErrorHandler } from ".";
import { GridInfo } from "./types";

export async function apiFetchMZoneReadings(
  mzone_id: number,
  mintimestamp: number = 0
): Promise<Either<string, Reading[]>> {
  return axios
    .get(`/api/readings/mzone/${mzone_id}?min=${mintimestamp}`)
    .then<Either<string, Reading[]>>(({ data }) => new Right(data.readings))
    .catch(apiErrorHandler);
}

export async function apiFetchSensorReadings(
  sensor_id: number,
  mintimestamp: number = 0
): Promise<Either<string, Reading[]>> {
  return axios
    .get(`/api/readings/sensors/${sensor_id}?min=${mintimestamp}`)
    .then<Either<string, Reading[]>>(({ data }) => new Right(data.readings))
    .catch(apiErrorHandler);
}

export async function apiUpdateSensor(sensor: Sensor): Promise<Either<string, Sensor>> {
  return axios
    .put("/api/sensors", sensor)
    .then<Either<string, Sensor>>(({ data }) => new Right(Sensor.fromJson(data.sensor)))
    .catch(apiErrorHandler);
}

export async function apiUpdateSZone(szone: SZone): Promise<Either<string, SZone>> {
  return axios
    .put("/api/grids/szone", szone)
    .then<Either<string, SZone>>(({ data }) => new Right(SZone.fromJson(data.szone)))
    .catch(apiErrorHandler);
}

export async function apiUpdateMZone(mzone: MZone): Promise<Either<string, MZone>> {
  return axios
    .put("/api/grids/mzone", mzone)
    .then<Either<string, MZone>>(({ data }) => new Right(MZone.fromJson(data.mzone)))
    .catch(apiErrorHandler);
}

export async function fetchUserGridPermissions(): Promise<Either<string, UserPermissions>> {
  return axios
    .get(`/api/users/permissions`)
    .then<Either<string, UserPermissions>>(({ data }) => new Right(data.permissions))
    .catch(apiErrorHandler);
}

export async function apiGenerateInviteLink(
  grid_id: number,
  permissions: number
): Promise<Either<string, string>> {
  return axios
    .post(`/api/invite/${grid_id}`, permissions)
    .then<Either<string, string>>(({ data }) => new Right(data.link))
    .catch(apiErrorHandler);
}

export async function createNewGridInstance(name: string): Promise<Either<string, Grid>> {
  return axios
    .post("/api/grids", { name })
    .then<Either<string, Grid>>(({ data }) => new Right(data.grid))
    .catch(apiErrorHandler);
}

export async function fetchGridInfos(): Promise<Either<string, GridInfo[]>> {
  return axios
    .get("/api/grids")
    .then<Either<string, GridInfo[]>>(({ data }) => new Right(data.grids))
    .catch(apiErrorHandler);
}

export async function fetchGridById(id: number): Promise<Either<string, Grid>> {
  return axios
    .get(`/api/grids/${id}`)
    .then<Either<string, Grid>>(({ data }) => new Right(Grid.fromJson(data.grid)))
    .catch(apiErrorHandler);
}

export async function putUpdateGrid(grid: Grid): Promise<Either<string, Grid>> {
  return axios
    .put("/api/grids", grid)
    .then<Either<string, Grid>>(({ data }) => new Right(data.grid))
    .catch(apiErrorHandler);
}
export async function apiDeleteMZone(mzone_id: number): Promise<Either<string, Grid>> {
  return axios
    .delete(`/api/grids/mzones/${mzone_id}`)
    .then<Either<string, Grid>>(({ data }) => new Right(Grid.fromJson(data.grid)))
    .catch(apiErrorHandler);
}

export async function apiDeleteRule(ruleId: number): Promise<Either<string, Rule>> {
  return axios
    .delete(`/api/rules/${ruleId}`)
    .then<Either<string, Rule>>(({ data }) => new Right(Rule.fromJson(data.rule)))
    .catch(apiErrorHandler);
}

export async function deleteGrid(id: number): Promise<Either<string, []>> {
  return axios
    .delete(`/api/grids/${id}`)
    .then<any>(() => new Right([]))
    .catch(apiErrorHandler);
}

export async function apiDeleteSZone(id: number): Promise<Either<string, Grid>> {
  return axios
    .delete(`/api/grids/szones/${id}`)
    .then<Either<string, Grid>>(({ data }) => new Right(Grid.fromJson(data.grid)))
    .catch(apiErrorHandler);
}

export async function apiDeleteSensor(id: number): Promise<Either<string, Sensor>> {
  return axios
    .delete(`/api/sensors/${id}`)
    .then<Either<string, Sensor>>(({ data }) => new Right(Sensor.fromJson(data.sensor)))
    .catch(apiErrorHandler);
}

export async function apiInsertSZone(gridId: number, szone: SZone): Promise<Either<string, SZone>> {
  return axios
    .post(`/api/grids/${gridId}/szones`, szone)
    .then<Either<string, SZone>>(({ data }) => new Right(SZone.fromJson(data.szone)))
    .catch(apiErrorHandler);
}

export async function apiInsertMZone(
  grid_id: number,
  mzone_name: string
): Promise<Either<string, MZone>> {
  return axios
    .post(`/api/grids/${grid_id}/mzones`, { grid_id, mzone_name, colour: "#87cefa" })
    .then<Either<string, MZone>>(({ data }) => new Right(MZone.fromJson(data.mzone)))
    .catch(apiErrorHandler);
}

export async function apiUpdateGridDimension(
  gridId: number,
  rect: Rect
): Promise<Either<string, []>> {
  return axios
    .put(`/api/grids/${gridId}/size`, rect)
    .then<Either<string, []>>(() => new Right([]))
    .catch(apiErrorHandler);
}

export async function apiFetchSensorsForGrid(gridId: number): Promise<Either<string, Sensor[]>> {
  return axios
    .get(`/api/sensors/${gridId}`)
    .then<Either<string, Sensor[]>>(({ data }) => new Right(data.sensors.map(Sensor.fromJson)))
    .catch(apiErrorHandler);
}

export async function apiInsertSensor(sensor: Sensor): Promise<Either<string, Sensor>> {
  return axios
    .post(`/api/sensors`, sensor)
    .then<Either<string, Sensor>>(({ data }) => new Right(Sensor.fromJson(data.sensor)))
    .catch(apiErrorHandler);
}
