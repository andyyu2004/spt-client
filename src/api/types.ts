export interface User {
  userid: number;
  username: string;
}

export interface GridInfo {
  grid_id: number;
  grid_name: string;
}

export interface ApiKey {
  apikey: string;
  hash: Uint8Array;
  grid_name: string;
  grid_id: number;
  creator_id: number;
}

