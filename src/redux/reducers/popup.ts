import { ReactElement } from "react";

export type PopupState = {
  content?: ReactElement;
};

export type ReduxPopupAction = SetPopupContentAction;
export type SetPopupContentAction = {
  type: "SET_POPUP_CONTENT";
  content?: ReactElement;
};

export const closePopup = () => setPopupContent(undefined);

export const setPopupContent = (content?: ReactElement): SetPopupContentAction => ({
  type: "SET_POPUP_CONTENT",
  content,
});

const initialState = {
  content: undefined,
};

const reducer = (state: PopupState = initialState, action: ReduxPopupAction): PopupState => {
  switch (action.type) {
    case "SET_POPUP_CONTENT": {
      const { content } = action;
      return { ...state, content };
    }
    default:
      return state;
  }
};

export default reducer;
