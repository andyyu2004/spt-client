import { combineReducers } from "redux";
import user, { UserState } from "./user";
import graphics, { GraphicsState } from "./graphics";
import popup, { PopupState } from "./popup";
import grid, { GridState } from "./grid";

export const rootReducer = combineReducers<AppState>({
  user,
  graphics,
  popup,
  grid,
});

export type AppState = {
  user: UserState;
  graphics: GraphicsState;
  popup: PopupState;
  grid: GridState;
};
