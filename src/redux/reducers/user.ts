import { Reducer } from "redux";
import { User } from "../../api/types";

/** grid_id -> permissions */
export type UserPermissions = {
  [key: number]: number;
};

export type UserState = {
  currentUser?: User;
  permissions: UserPermissions;
};

export type ReduxUserAction =
  | SetCurrentUserAction
  | LogoutCurrentUserAction
  | SetUserPermissionsAction;

export type SetCurrentUserAction = {
  type: "SET_CURRENT_USER";
  user: User;
};
export const setCurrentUser = (user: User): SetCurrentUserAction => ({
  type: "SET_CURRENT_USER",
  user,
});

export type SetUserPermissionsAction = {
  type: "SET_USER_PERMISSIONS";
  permissions: UserPermissions;
};

export const setUserPermissions = (permissions: UserPermissions): SetUserPermissionsAction => ({
  type: "SET_USER_PERMISSIONS",
  permissions,
});

export type LogoutCurrentUserAction = {
  type: "LOGOUT_CURRENT_USER";
};
export const logoutCurrentUser = (): LogoutCurrentUserAction => ({ type: "LOGOUT_CURRENT_USER" });

const defaultState = {
  currentUser: undefined,
  permissions: {},
};

const userReducer: Reducer<UserState, ReduxUserAction> = (state = defaultState, action) => {
  switch (action.type) {
    case "SET_CURRENT_USER": {
      const { user } = action;
      return { ...state, currentUser: user };
    }
    case "SET_USER_PERMISSIONS":
      const { permissions } = action;
      return { ...state, permissions };

    case "LOGOUT_CURRENT_USER":
      return { ...state, currentUser: undefined };
    default:
      return state;
  }
};

export default userReducer;
