import { Value } from "react-svg-pan-zoom";

export type GraphicsState = {
  value: Value;
};

export type ReduxGraphicsAction = SetValueAction;

export type SetValueAction = {
  type: "SET_VALUE";
  value: Value;
};

export const reduxSetValue = (value: Value): SetValueAction => ({
  type: "SET_VALUE",
  value,
});

const initialState = {
  value: { a: 1 } as Value,
};

const graphicsReducer = (
  state: GraphicsState = initialState,
  action: ReduxGraphicsAction
) => {
  switch (action.type) {
    case "SET_VALUE": {
      const { value } = action;
      return { ...state, value };
    }
    default:
      return state;
  }
};

export default graphicsReducer;
