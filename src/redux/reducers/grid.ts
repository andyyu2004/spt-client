import { GridAction } from "../../views/layout/actions";
import gridReducerWrapper from "../../views/layout/gridReducer";
import { Grid } from "../../views/layout/types";

export type GridState = Grid;

const initialState = Grid.default();

const reducer = (state: GridState = initialState, action: GridAction) =>
  gridReducerWrapper(state, action);

export default reducer;
