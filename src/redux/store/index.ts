import { createStore, applyMiddleware } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // allows login indefinitely until explicit logout
// import storage from 'redux-persist/lib/storage/session'; // allows login until browser closed
import { rootReducer } from "../reducers";
import logger from "redux-logger";

const persistConfig = {
  key: "root",
  blacklist: ["graphics", "popup"],
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
export const store = createStore(persistedReducer, applyMiddleware(logger));
export const persistor = persistStore(store);
