import { Router } from "@reach/router";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import styles from "./app.module.scss";
import { useUserState } from "./hooks";
import Main from "./Main";
import { logoutCurrentUser } from "./redux/reducers/user";
import { Login, Signup } from "./views";
import { checkAuth } from "./api/users";

/** the component that contains the application in a not logged in state */
const PreLogin = () => {
  return (
    <div className="flex-container">
      <Router className="flex-container">
        <Signup path="/register" />
        <Login path="*" />
      </Router>
    </div>
  );
};

const App = () => {
  const isLoggedIn = useUserState().currentUser !== undefined;
  const dispatch = useDispatch();

  // runs a double check on first render of app if user is indeed authorized by server
  useEffect(() => {
    checkAuth().then(either =>
      either
        .map(isAuth => {
          if (!isAuth) {
            dispatch(logoutCurrentUser());
          }
          return isAuth;
        })
        .mapLeft(toast.error)
    );
  }, [dispatch]);

  return (
    <div>
      <ToastContainer autoClose={1500} pauseOnHover={false} />
      <div className={styles.app}>{isLoggedIn ? <Main /> : <PreLogin />}</div>
    </div>
  );
};

export default App;
