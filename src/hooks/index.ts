import { useSelector, useDispatch } from "react-redux";
import { AppState } from "../redux/reducers";
import { UserPermissions, UserState } from "../redux/reducers/user";
import { GridState } from "../redux/reducers/grid";
import { useCallback, useEffect } from "react";
import { setGrid } from "../views/layout/actions";
import { compose } from "../util/functional";
import { fetchGridById } from "../api";
import { toast } from "react-toastify";
import { navigate } from "@reach/router";

export const useUserState = () => useSelector<AppState, UserState>(state => state.user);
export const useGrid = () => useSelector<AppState, GridState>(state => state.grid);

// sets the grid in redux store
export const useFetchGrid = (gridId: number) => {
  const dispatch = useDispatch();

  const fetchAndSetGrid = useCallback(async () => {
    console.log(`fetching grid ${gridId}`);
    (await fetchGridById(gridId!)).map(compose(dispatch, setGrid)).mapLeft(err => {
      toast.error(err);
      navigate("/sites/");
    });
  }, [gridId, dispatch]);

  useEffect(() => {
    fetchAndSetGrid();
  }, [fetchAndSetGrid]);
};

export const usePermissions = (gridId: number): number => {
  const userState = useSelector<AppState, UserState>(state => state.user);
  const gridPermissions = gridId in userState.permissions ? userState.permissions[gridId] : 0;
  const permissions = userState.currentUser ? gridPermissions : 0;
  return permissions;
};

export const useUserPermissions = (): UserPermissions => {
  const userState = useSelector<AppState, UserState>(state => state.user);
  return userState.permissions;
};
