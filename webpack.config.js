
module.exports = {
    module : {
	rules: [
	    {
	      type: 'javascript/auto',
	      test: /\.mjs$/,
	      use: []
	    }
	]
    },
    resolve: {
	alias: {
	    "@remotelock/react-week-scheduler": "@remotelock/react-week-scheduler/lib.js"
	}
    }
};
