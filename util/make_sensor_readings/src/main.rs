use chrono::Duration;
use rand::prelude::*;
use serde::Serialize;
use std::io;

const KEY: &str = "Ap-FS5fSIpQOo6tvf0ufSKed1cID_QK5";

#[derive(Serialize, Debug)]
struct Reading {
    sensor_id: i32,
    ctime: u64,
    moisture: i16,
}

fn main() -> io::Result<()> {
    let time = chrono::Utc::now();
    let min_time = time.checked_sub_signed(Duration::weeks(1)).unwrap().timestamp();
    for _ in 0..1000 {
        let rand_time = rand::thread_rng().gen_range(min_time, time.timestamp());
        make_sensor_readings(
            "http://localhost:3001/api/readings",
            KEY,
            Reading {
                sensor_id: 1,
                ctime: rand_time as u64,
                moisture: rand::thread_rng().gen_range(500, 800),
            },
        )?;
    }

    Ok(())
}

fn make_sensor_readings(uri: &str, key: &str, body: impl Serialize) -> io::Result<()> {
    let body = serde_json::to_value(body).unwrap();
    let res = ureq::post(uri).set("X-API-KEY", key).send_json(body);
    if res.ok() {
        println!("success: {}", res.into_string()?);
    } else {
        println!("error {}: {}", res.status(), res.into_string()?);
    };
    Ok(())
}
